﻿using System.Reflection;
using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using LandsWa.Acceptance.Smoke.Tests.Resources;
using System.Linq;
using log4net.Config;
using LandsWa.Acceptance.Smoke.Tests.Training;

namespace LandsWa.Acceptance.Smoke.Tests.SetupTeardown
{
    [TestFixture]
    public class BaseTest
    {
        public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected IWebDriver Driver { get; private set; }
        protected LoginPage loginPage;
        protected MyDashboardPage myDashboard;
        protected TeamDashboardPage teamDashboard = null;
        protected ApplicantDetailsMileStone applicantDetailsPage = null;
        protected RequestDetailsMileStone requestDetailsPage = null;
        protected LandDetailsMileStone landDetailsPage = null;
        protected ConsultationMileStone consultationPage = null;
        protected AdditionalInformationMileStone additionalInfoPage = null;
        protected ReviewMileStone reviewPage = null;
        protected SubmissionMileStone submissionPage = null;
        protected CaseDashboardPage caseDashboard = null;
        protected AllocateNewCasePage allocateNewCase = null;
        protected ProceedWithCasePage proceedWithCase = null;
        protected CalculatePriorityRatingPage calculatePriorityRatingPage = null;
        protected ApproveOrAmendPriorityRatingPage approveOrAmendPriorityRatingPage = null;
        protected IntegrateWithObjectivePage integrateWithObjectivePage = null;
        protected CreateTaskPage createTaskPage = null;
        protected FinaliseCasePage finaliseCase = null;
        protected string failedCasesFilePath = null;
        protected string appSubDataFile;
        protected string landRecordDataFile;
        protected int StartDataRowNumber; //CSV Line Number
        protected int EndDataRowNumber; //CSV Line Number
        protected List<AppSubTestData> AppSubTestDataList { get; set; }
        protected List<LandRecordTestData> LandRecordTestDataList { get; set; }
        protected AppSubTestData row = null;
        protected int ArrayCsvIndexOffsetValue = 2; //CSV row is 2 places ahead in the Data Array
        protected TestWorkFlowHelper WorkFlowHelper;

        [OneTimeSetUp]
        public void TestSuiteSetup()
        {
            Driver = GetLocalDriver(BrowserType.Chrome);
            Driver.Manage().Window.Maximize();
            BasePage.env = Env.Test;
            failedCasesFilePath = $"{ BasePage.GetFolderPathInProjectRoot("Output")}failed.txt";
            failedCasesFilePath = Path.GetFullPath(@failedCasesFilePath);
            FileInfo fileInfo = new FileInfo(failedCasesFilePath);

            if (!fileInfo.Exists)
                Directory.CreateDirectory(fileInfo.Directory.FullName);

            using (var tw = new StreamWriter(failedCasesFilePath, true))
            {
                tw.WriteLine($"Execution Date and Time: {DateTime.Now.ToString("yyyyMMddHHmmss")}");
            }
            //IEnumerable<string> allFirstNames = AppSubTestDataList.Select(AppSubTestData => AppSubTestData.UserName);
            XmlConfigurator.Configure();
        }

        [SetUp]
        public virtual void SetUp()
        {
            Driver.Manage().Cookies.DeleteAllCookies();
            appSubDataFile = "TestData_v8.0.csv";
            //appSubDataFile = "RealAccounts_12thNov.csv";
            landRecordDataFile = "LandRecordDetails.csv";
            AppSubTestDataList = AppSubTestData.GetAppSubTestData(appSubDataFile);
            LandRecordTestDataList = LandRecordTestData.GetLandRecordTestData(landRecordDataFile);
            WorkFlowHelper = new TestWorkFlowHelper(Driver);

        }

        [TearDown]
        public void TearDown()
        {
            BasePage.CaseNumber = "NOT ASSIGNED";

            int endIndex = 0;
            string path = BasePage.GetFolderPathInProjectRoot("ss");
            if (TestContext.CurrentContext.Test.Name.Length > 50)
                endIndex = 50;
            else
                endIndex = TestContext.CurrentContext.Test.Name.Length;
            string method = String.Join("", Regex.Unescape(TestContext.CurrentContext.Test.Name).Split('\"')).Substring(0, endIndex);
            path = $@"{path}{method}.png";
            BasePage.TakeScreenshot(path);
            Logger.WriteLog($"Screenshot taken: {path}");
        }

        [OneTimeTearDown]
        public void TestTeardown()
        {
            try
            {
                
                CleanUpInstances();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
            }
        }

        private IWebDriver GetLocalDriver(BrowserType browserType)
        {
            switch (browserType)
            {
                case BrowserType.Chrome:
                    /**
                     * ChromeOptions options = new ChromeOptions();
                     * this path needs to be setup on TeamCity build agent as well - about:version
                     * options.AddArguments(@"user-data-dir=C:\Chromium\Temp\Profile\Default");
                     * driver = new ChromeDriver(options);
                     **/
                    ChromeOptions options = new ChromeOptions();
                    //options.AddArguments(@"user-data-dir=C:\Chromium\Temp\Profile\Default");
                    //Driver = new ChromeDriver(options);
                    Driver = new ChromeDriver();
                    break;
                case BrowserType.FireFox:
                    Driver = new FirefoxDriver();
                    break;
                case BrowserType.InternetExplorer:
                    Driver = new InternetExplorerDriver();
                    break;
            }
            return Driver;
        }

        public void CleanUpInstances()
        {
            if (Driver != null)
            {
                Driver.Dispose();
                Driver = null;
            }
        }
    }
}
