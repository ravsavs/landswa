﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Helper
{
    public class Enumerations
    {
        public enum Env
        {
            Test,
            PreProd,
            Prod,
            Dev
        }
        /// <summary>
        /// All different Browsers supported by the framework
        /// </summary>
        public enum BrowserType
        {
            Chrome,
            FireFox,
            InternetExplorer
        }

        public enum User
        {
            Officer,
            Manager,
            ExecutiveDirector
        }

        public enum Decision
        {
            Yes,
            No,
            NotApplicable
        }

        public enum CaseType
        {
            Internal,
            External
        }

        public enum CustomerAssociatedWithCase
        {
            Yes,
            No
        }

        public enum IsLandRecordAssociatedWithThisInternalCase
        {
            Yes,
            No
        }


        public enum RequestType
        {
            General,
            LGA,
            Event
        }

        public enum ContactMethod
        {
            Email,
            PrintAndPost,
            BothEmailAndPrintAndPost
        }

        public enum CaseAllocationAction
        {
            WithinTeam,
            OutsideTeam
        }

        public enum RelatedActions
        {
            IntegrateWithObjective,
            CalculatePriorityRating,
            AddCaseNotes,
            UpdateCaseDetails,
            CreateTask,
            FinaliseCase


        }
    }
}
