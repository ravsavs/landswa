﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests.Create_Task_Tests
{
    public class CreateTaskTest : BaseTest
    {
        LoginPage loginPage;
        //TeamDashboardPage teamDashboard;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        CreateTaskPage createTask;

        [TestCase("AvaKnP", "Ava", "infy4321", "1813030",
    "Ava", "1928", "Tenure Search","Please perform this task.", "20181001 TaskDoc.obr",
    "Specify Tenure Search Items to Perform")]
        public void VerifyUserCanCreateandAssignTask
    (string userLogin, string userName, string password, string caseNumber,
     string allocatedOfficerName, string WI, string taskName, string comments, string fileName,
     string newTaskName)
        {
            loginPage = new LoginPage(Driver);

            //Login as user who will do the Priority Rating
            myDashboard = loginPage.LogIn(userLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));

            //Filter and open case dashboard
            myDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(myDashboard.IsCaseFoundInMyDashboard(caseNumber));
            caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());

            //Create Task
            createTask = (CreateTaskPage)caseDashboard.ClickRelatedActionFromRelatedActionTab(RelatedActions.CreateTask);
            Assert.IsTrue(createTask.IsPageLoadComplete());

            caseDashboard = createTask.EnterOfficerName(allocatedOfficerName)
                .EnterWI(WI)
                .EnterTaskName(taskName)
                .EnterComments(comments)
                .SelectNoUploadOption() //for not uploading document
                //.SelectYesUploadOptionAndUploadFile(fileName) //for uploading document
                .ClickDoneButton();
            caseDashboard.ClickSummaryTab();
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(newTaskName));            
        }
    }
}
