﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests.Case_Allocation_Test
{
    class ProceedWithCaseTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        ProceedWithCasePage proceedWithCase;
        string caseNumber = null;

        [TestCase("LiamKnP", "Liam", User.Officer, "infy4321", "Proceed With Case")]
        public void VerifyProceedWithCaseAfterAllocation
            (string userLogin, string userName, User user, string password, string actionTask)
        {
            loginPage = new LoginPage(Driver);

            caseNumber = BasePage.CaseNumber;
            Console.WriteLine(BasePage.CaseNumber);

            //Login as Officer and Proceed with case
            myDashboard = loginPage.LogIn(userLogin, password);

            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));

            myDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
            Assert.IsTrue(myDashboard.IsCaseFoundInMyDashboard(BasePage.CaseNumber));

            caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));

            proceedWithCase = (ProceedWithCasePage)caseDashboard.ClickTaskInOpenTasksGrid(actionTask);

            Assert.IsTrue(proceedWithCase.IsPageLoadComplete());
            Assert.IsTrue(proceedWithCase.IsProceedWithCaseTaskForNewCase(BasePage.CaseNumber));

            caseDashboard = proceedWithCase.ClickConfirmationCheckbox()               
                .ClickDoneButton();

            Assert.IsTrue(caseDashboard.IsPageLoaded());
        }
    }
}

