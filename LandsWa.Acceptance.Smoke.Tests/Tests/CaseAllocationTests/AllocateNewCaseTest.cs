﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests.Case_Allocation_Test
{
    class AllocateNewCaseTest : BaseTest
    {
        LoginPage loginPage;
        TeamDashboardPage teamDashboard;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        AllocateNewCasePage allocateNewCase;
        string caseNumber = null;

        [TestCase("LiamKnP", "Liam", "AvaKnP", "Ava" , "infy4321",
            CaseAllocationAction.WithinTeam, "1928","Automation - Allocating Case to officer in Team",
            "Allocate New Case")]
        public void VerifyAllocateNewCaseWithinTeamAfterSubmission
            (string officerLogin, string officerName, string managerLogin, string managerName, string password,
            CaseAllocationAction caseAllocationAction, string WI, string comments,
            string actionTask)
        {
            string dueDate = null;
            loginPage = new LoginPage(Driver);

               //Submit New Case
               loginPage.SubmitNewCase(
                     officerLogin, 
                     officerName, 
                     password, 
                     User.Officer, 
                     CaseType.External, 
                     "Svetlana", 
                     "Lease", 
                     "Kalamunda"
                     );

               caseNumber = BasePage.CaseNumber;
               Console.WriteLine(BasePage.CaseNumber);

               loginPage.LogOut();         

       
            //Login as Manager and Allocate New Case within team
            myDashboard = loginPage.LogIn(managerLogin, password);

            teamDashboard = myDashboard.NavigateToTeamDashboard();
            Assert.IsTrue(teamDashboard.IsPageLoadComplete());
            Assert.IsTrue(teamDashboard.IsManagerNameDisplayed(managerName));
            
            teamDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
            Assert.IsTrue(teamDashboard.IsNewCaseFoundInTeamDashboard(BasePage.CaseNumber));
              
            caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
             Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));

            allocateNewCase = (AllocateNewCasePage) caseDashboard.ClickTaskInOpenTasksGrid(actionTask);

            Assert.IsTrue(allocateNewCase.IsPageLoadComplete());
            Assert.IsTrue(allocateNewCase.IsAllocateNewCaseTaskForNewCase(BasePage.CaseNumber));

            caseDashboard = allocateNewCase.ClickAcceptTask()
                .SelectCaseAllocationAction(caseAllocationAction)
                .EnterOfficerName(officerName)
                .EnterWI(WI)
                .EnterComments(comments)
                .EnterDueDate(dueDate)
                .ClickDoneButton();
            
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard("Proceed With Case"));

            loginPage.LogOut();
        }
    }
}









