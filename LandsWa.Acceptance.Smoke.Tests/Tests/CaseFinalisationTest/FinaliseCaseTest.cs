﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    class FinaliseCaseTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;
        TeamDashboardPage teamDashboard;
        CaseDashboardPage caseDashboard;
        FinaliseCasePage finaliseCase;

        [TestCase("EllaLiv", "Ella", "infy4321",
            "1813547", "No response from applicant", "No response received despite numerous attempts to make contact")]
        public void VerifyManagerCanFinaliseCase
            (string userLogin, string userName, string password,
            string caseNumber, string finalisationReason, string finalisationComments)
        {
            loginPage = new LoginPage(Driver);

            //Login as manager who will Finalsie Case
            myDashboard = loginPage.LogIn(userLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));

            //Filter and open case dashboard from team dashboard
            teamDashboard = myDashboard.NavigateToTeamDashboard();
            Assert.IsTrue(teamDashboard.IsPageLoadComplete());
            teamDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(teamDashboard.IsNewCaseFoundInTeamDashboard(caseNumber));
            caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());


            //Finalise Case
            finaliseCase = (FinaliseCasePage)caseDashboard.ClickRelatedActionFromRelatedActionTab(RelatedActions.FinaliseCase);
            Assert.IsTrue(finaliseCase.IsPageLoadComplete());

            caseDashboard = finaliseCase.SelectFinalisationReason(finalisationReason)
                .EnterComments(finalisationComments)
                .SelectNotToNotifyApplicantOrCustomer()
                .TickMandatoryCheckboxes()
                .ClickDoneButton();          

            
        }
    }
}
