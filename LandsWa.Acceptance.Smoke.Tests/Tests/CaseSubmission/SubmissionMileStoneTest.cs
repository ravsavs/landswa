﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    class SubmissionMileStoneTest : BaseTest
    {
        [TestCase("LiamKnP", "Liam", "infy4321", User.Officer, CaseType.External, "Thomas", "Lease", "Joondalup")]
        public void VerifyThatAssOfficerIsAbleToPerformNewCaseSubmissionForExternalRequest(
            string login,
            string name,
            string password,
            User user,
            CaseType caseType,
            string applicantName,
            string category,
            string LGAName
            )
        {
            var loginPage = new LoginPage(Driver);

            var myDashboard = loginPage.LogIn(login, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(name));

            var page = (AssignApplicantCustomerPage)myDashboard.ClickCreateNewCaseButton()
                .InternalOrExternal(caseType)
                .ClickDoneButton();

                page.SearchAnApplicantWithName(applicantName)
                .SelectTheApplicantFromSearchResultWithName(applicantName)
                .Continue()
                .ClickContinueButton()
                .SelectGeneralRequestType()
                .SelectCategoryFromDropdown(category)
                .EnterDescription("Descr")
                .ClickCLEFRequestCheckbox()
                .ClickApplicantSignedCheckbox()
                .EnterDateSigned()
                .EnterDateReceived()
                .UploadDocument("Document_1.txt")
                .ClickContinueButton()
                .AddLandRecordForLGA(LGAName)
                .ClickContinueButton()
                .ClickLGACheckboxToConsult()
                .HasLGABeenConsultedRadioButtonResponse(Decision.Yes)
                .ClickUpdateButton()
                .ClickContinueButton()
                .ClickContinueButton()
                .ClickCheckBox()
                .ClickSubmitButton()
                .SendCaseSummaryToApplicantRadioButton(Decision.Yes)
                .SelectMethodOfContact(ContactMethod.Email)
                .AnyOtherDocumentsToSend(Decision.No)
                .ClickReadyToEmailConfirmationCheckbox()
                .ClickDoneButton();
        }

        [TestCase("LiamKnP", "Liam", "infy4321", User.Officer, CaseType.Internal, "Albert", 
            CustomerAssociatedWithCase.Yes, "General Enquiry - by Email", IsLandRecordAssociatedWithThisInternalCase.No,"Kalamunda", 
            Decision.No, "Health")]
        public void VerifyThatAssOfficerIsAbleToPerformNewCaseSubmissionForInternalRequest(
            string login, string name, string password, User user, CaseType caseType, string customerName, 
            CustomerAssociatedWithCase customerAssociated, string category, IsLandRecordAssociatedWithThisInternalCase landWithInternalCase ,string LGAName,
            Decision otherAgencyWithInternalCase, string agencyName)
        {
            var loginPage = new LoginPage(Driver);

            var myDashboard = loginPage.LogIn(login, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(name));

            var assignApplicantPage = (AssignApplicantCustomerPage)myDashboard.ClickCreateNewCaseButton()
                .InternalOrExternal(caseType)
                .IsCustomerAssociatedWithCase(customerAssociated)
                .ClickDoneButton();

            var applicantDetailsPage = assignApplicantPage.SearchAnApplicantWithName(customerName)
               .SelectTheApplicantFromSearchResultWithName(customerName)
               .Continue();

            var consultationPage = (ConsultationMileStone)applicantDetailsPage.UploadDocument("Document_1.txt")
                .ClickContinueButton()
                .SelectCategoryFromDropdown(category)
                .EnterDescription("Automated Testing")
                .EnterCaseDescription("Internal Automated Case With a Customer")
                .ClickApplicantSignedCheckbox()
                .EnterDateSigned()
                .EnterDateReceived()
                .UploadDocument("Document_1.txt")
                .ClickContinueButton()
                .AreLandRecordsAssociatedWithThisInternalCase(landWithInternalCase)
                .AddLandRecordForLGA(LGAName)
                .ClickContinueButton();

            if (landWithInternalCase.ToString().Equals("Yes"))
            {
                consultationPage.ClickLGACheckboxToConsult()
                .HasLGABeenConsultedRadioButtonResponse(Decision.Yes)
                .ClickUpdateButton();
            }

            consultationPage.AreOtherAgenciesConsultedWithThisInternalCase(otherAgencyWithInternalCase)
                .ClickOnAddOtherAgencyImage()
                .AddOtherAgency(agencyName)
                .EnterOutcome("Approved")
                .ClickContinueButton()
                .ClickContinueButton()
                .ClickCheckBox()
                .ClickSubmitButton()
                .SendCaseSummaryToApplicantRadioButton(Decision.Yes)
                .SelectMethodOfContact(ContactMethod.Email)
                .AnyOtherDocumentsToSend(Decision.No)
                .ClickReadyToEmailConfirmationCheckbox()
                .ClickDoneButton();
        }

        [TestCase("LiamKnP", "Liam", "infy4321", User.Officer, CaseType.Internal, "Albert",
            CustomerAssociatedWithCase.No, "General Enquiry - by Email", IsLandRecordAssociatedWithThisInternalCase.Yes, "Kalamunda",
            Decision.Yes, "Health")]
        public void VerifyThatOfficerIsAbleToPerformNewCaseSubmissionForInternalRequestWithoutCustomer(
            string login, string name, string password, User user, CaseType caseType, string customerName,
            CustomerAssociatedWithCase customerAssociated, string category, IsLandRecordAssociatedWithThisInternalCase landWithInternalCase, string LGAName,
            Decision otherAgencyWithInternalCase, string agencyName)
        {
            var loginPage = new LoginPage(Driver);

            var myDashboard = loginPage.LogIn(login, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(name));

            var applicantDetailsPage = (ApplicantDetailsMileStone)myDashboard.ClickCreateNewCaseButton()
                .InternalOrExternal(caseType)
                .IsCustomerAssociatedWithCase(customerAssociated)
                .ClickDoneButton();

            var landDetailsPage = (LandDetailsMileStone)applicantDetailsPage.ClickContinueButton()
                .SelectCategoryFromDropdown(category)
                .EnterDescription("Automated Testing- Internal Case Without a Customer")
                .ClickApplicantSignedCheckbox()
                .EnterDateSigned()
                .EnterDateReceived()
                .ClickContinueButton();

            var consultationPage =   (ConsultationMileStone) landDetailsPage.AreLandRecordsAssociatedWithThisInternalCase(landWithInternalCase)
                .AddLandRecordForLGA(LGAName)
                .ClickContinueButton();

            if (landWithInternalCase.ToString().Equals("Yes"))
            {
                consultationPage.ClickLGACheckboxToConsult()
                .HasLGABeenConsultedRadioButtonResponse(Decision.Yes)
                .ClickUpdateButton();
            }

            consultationPage.AreOtherAgenciesConsultedWithThisInternalCase(otherAgencyWithInternalCase)
                .ClickOnAddOtherAgencyImage()
                .AddOtherAgency(agencyName)
                .EnterOutcome("Approved")
                .ClickContinueButton()
                .ClickContinueButton()
                .ClickCheckBox()
                .ClickSubmitButton()
                .ClickDoneButton();
        }
    }
}
