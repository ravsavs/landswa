﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    public class LoginPageTests : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;

        [TestCase("SophiaAss", "Sophia", "infy4321", User.Manager)]
        [TestCase("BenAss", "Ben", "infy4321", User.Officer)]
        public void VerifyThatUserCanLogin(string login, string name, string password, User user)
        {
            loginPage = new LoginPage(Driver);

            myDashboard = loginPage.LogIn(login, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(name));
        }
    }
}
