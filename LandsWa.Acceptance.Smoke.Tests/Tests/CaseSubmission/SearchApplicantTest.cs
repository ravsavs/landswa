﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    public class SearchApplicantTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;

        [TestCase("BenAss", "Ben", "infy4321", User.Officer, "Ravi")]
        public void VerifyThatAnOfficerCanSearchAnApplicantLogin(string login, string name, string password, User user, string applicantName)
        {
            loginPage = new LoginPage(Driver);

            myDashboard = loginPage.LogIn(login, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(name));

            //myDashboard.ClickCreateNewCaseButton()
            //    .SearchAnApplicantWithName(applicantName)
            //    .SelectTheApplicantFromSearchResultWithName(applicantName)
            //    .Continue();

        }
    }
}
