﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;
using LandsWa.Acceptance.Smoke.Tests.Resources;
using System.IO;
using LandsWa.Acceptance.Smoke.Tests.Training;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    class ApproveOrAmendPriorityRatingTest : BaseTest
    {
        [SetUp]
        public override void SetUp()
        {
            Driver.Manage().Cookies.DeleteAllCookies();
            appSubDataFile = "TestData.csv";
            //appSubDataFile = "RealAccounts_12thNov.csv";
            landRecordDataFile = "LandRecordDetails.csv";
            AppSubTestDataList = AppSubTestData.GetAppSubTestData(appSubDataFile);
            LandRecordTestDataList = LandRecordTestData.GetLandRecordTestData(landRecordDataFile);
            WorkFlowHelper = new TestWorkFlowHelper(Driver);

        }

        [Test]
        public void CreateTrainingData()
        {
            try
            {
                row = AppSubTestDataList[2];
                WorkFlowHelper.CreateWorkFlow(row);
            }
            catch (Exception e)
            {
                Logger.WriteLog($"CASE CREATION FAILED - {row.SerialNumber}; Case NUMBER {BasePage.CaseNumber}");
                Logger.WriteLog(e.Message + "\n" + e.StackTrace);
            }
        }

        [TestCase("EllaLiv", "Ella", "infy4321", "Approve or Amend Priority Rating",
            "Amend", "Government stakeholder", "Automation - Amended Priority Rating" )]
        public void VerifyManagerCanApproveOrAmendPriorityRating
            (string managerLogin, string managerName, string password, string actionTask,
            string managerPriorityDecision, string managerPriorityOption, string managerPriorityComments)
        {
            row = AppSubTestDataList[3];
            WorkFlowHelper.CreateWorkFlow(row);
            string caseNumber = BasePage.CaseNumber;

            loginPage = new LoginPage(Driver);

            //Login as user who will Amend/Approve the Priority Rating
            myDashboard = loginPage.LogIn(managerLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(managerName));

            //Filter and open case dashboard from team dashboard
            teamDashboard = myDashboard.NavigateToTeamDashboard();
            Assert.IsTrue(teamDashboard.IsPageLoadComplete());
            teamDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(teamDashboard.IsNewCaseFoundInTeamDashboard(caseNumber));
            caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));

            //Amend or Approve Priority Rating
            approveOrAmendPriorityRatingPage = (ApproveOrAmendPriorityRatingPage) caseDashboard.ClickTaskInOpenTasksGrid(actionTask);

            Assert.IsTrue(approveOrAmendPriorityRatingPage.IsPageLoadComplete());
            Assert.IsTrue(approveOrAmendPriorityRatingPage.IsPriorityApprovalTaskForCaseDisplayed(caseNumber));

            approveOrAmendPriorityRatingPage.ClickAcceptTask()
                .SelectPriorityRatingDecision(managerPriorityDecision)
                .EnterManagerComments(managerPriorityComments);

            if (managerPriorityDecision == "Amend")
            {
                approveOrAmendPriorityRatingPage.EditPriorityRating(managerPriorityOption);
            }

            approveOrAmendPriorityRatingPage.ClickDoneButton();
                                            
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard("Notification of Approved Priority Rating"));            
        }

        [TestCase("AvaKnP", "Ava", "infy4321",
           "1813030", "Approve or Amend Priority Rating")]
        public void VerifyManagerCanSaveExitApproveOrAmendPriorityRating
           (string managerLogin, string managerName, string password,
           string caseNumber, string actionTask)
        {
            loginPage = new LoginPage(Driver);

            //Login as user who will Amend/Approve the Priority Rating
            myDashboard = loginPage.LogIn(managerLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(managerName));

            //Filter and open case dashboard from team dashboard
            teamDashboard = myDashboard.NavigateToTeamDashboard();
            Assert.IsTrue(teamDashboard.IsPageLoadComplete());
            teamDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(teamDashboard.IsNewCaseFoundInTeamDashboard(caseNumber));
            caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));

            //Amend or Approve Priority Rating
            approveOrAmendPriorityRatingPage = (ApproveOrAmendPriorityRatingPage)caseDashboard.ClickTaskInOpenTasksGrid(actionTask);

            Assert.IsTrue(approveOrAmendPriorityRatingPage.IsPageLoadComplete());
            Assert.IsTrue(approveOrAmendPriorityRatingPage.IsPriorityApprovalTaskForCaseDisplayed(caseNumber));

            approveOrAmendPriorityRatingPage.ClickAcceptTask()
                .ClickSaveAndExitButton();

            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));
        }


    }
}
