﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests.Priority_Rating_Tests
{
    class CalculatePriorityRatingTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        CalculatePriorityRatingPage calculatePriorityRating;
        //string caseNumber = null;

        [TestCase("LiamKnP", "Liam", "infy4321",
            "1813036", "1928", "Lead Agency Project")]
        public void VerifyUserCanDoCalculatePriorityRating
            (string userLogin, string userName, string password,
            string caseNumber, string workInstruction, string priorityOption)
        {
            loginPage = new LoginPage(Driver);

            //Login as user who will do the Priority Rating
            myDashboard = loginPage.LogIn(userLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));


            //Filter and open case dashboard
            myDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(myDashboard.IsCaseFoundInMyDashboard(caseNumber));
            caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());

            //Calculate Priority Rating
            calculatePriorityRating = (CalculatePriorityRatingPage)caseDashboard.ClickRelatedActionFromRelatedActionTab(RelatedActions.CalculatePriorityRating);
            Assert.IsTrue(calculatePriorityRating.IsPageLoadComplete());

            caseDashboard = calculatePriorityRating.EnterWI(workInstruction)
                .SelectPriorityOption(priorityOption)
                .ClickDoneButton()
                .ClickSummaryTab();
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard("Approve or Amend Priority Rating"));
        }
    }
   
}
