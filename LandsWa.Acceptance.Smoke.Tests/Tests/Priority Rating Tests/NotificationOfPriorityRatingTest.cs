﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    class NotificationOfPriorityRatingTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        NotificationOfPriorityRatingPage notificationTask;

        [TestCase("LiamKnP", "Liam", "infy4321",
         "1813026", "Notification of Approved Priority Rating")]
        public void VerifyUserAcceptsPriorityNotificationTask
         (string userLogin, string userName, string password,
            string caseNumber, string actionTask)
        {
            loginPage = new LoginPage(Driver);

            //Login as user to accept the priority rating notification task
            myDashboard = loginPage.LogIn(userLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));
            //Filter and open case dashboard
            myDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(myDashboard.IsCaseFoundInMyDashboard(caseNumber));
            caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());
            Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard(actionTask));

            notificationTask = (NotificationOfPriorityRatingPage) caseDashboard.ClickTaskInOpenTasksGrid(actionTask);

            Assert.IsTrue(notificationTask.IsPageLoadComplete());
            Assert.IsTrue(notificationTask.IsNotificationTaskForCase(caseNumber));

            caseDashboard = notificationTask.ClickOKButton();

            Assert.IsTrue(caseDashboard.IsPageLoaded());




        }
    }
}
