﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    public class IntegrateWithObjectiveTest : BaseTest
    {
        LoginPage loginPage;
        MyDashboardPage myDashboard;
        CaseDashboardPage caseDashboard;
        IntegrateWithObjectivePage integrateWithObjective;
        
        [TestCase("EllaLiv", "Ella", "infy4321",
            "1813061", "L00043-2017","Integrating Case with Objective")]
        public void VerifyUserCanIntegrateCaseWithObjective
            (string userLogin, string userName, string password,
            string caseNumber, string fileNumber, string integrationComments)
        {
            loginPage = new LoginPage(Driver);

            //Login as user who will do the Priority Rating
            myDashboard = loginPage.LogIn(userLogin, password);
            Assert.IsTrue(myDashboard.IsPageLoadComplete());
            Assert.IsTrue(myDashboard.IsOfficerNameDisplayed(userName));


            //Filter and open case dashboard
            myDashboard.FilterAndSearchByCaseNumber(caseNumber);
            Assert.IsTrue(myDashboard.IsCaseFoundInMyDashboard(caseNumber));
            caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(caseNumber);
            Assert.IsTrue(caseDashboard.IsPageLoadComplete());

            //IntegrateWithObjective
            integrateWithObjective = (IntegrateWithObjectivePage)caseDashboard.ClickRelatedActionFromRelatedActionTab(RelatedActions.IntegrateWithObjective);
            Assert.IsTrue(integrateWithObjective.IsPageLoadComplete());

            caseDashboard = integrateWithObjective.EnterFileNumber(fileNumber)
                .SelectNoCaseFolderOption()
                .EnterComments(integrationComments)
                .ClickIntegrateButton()
                .ClickDoneButton();

            caseDashboard.ClickSummaryTab();

            Assert.IsTrue(caseDashboard.IsCaseIntegratedWithObjective(fileNumber));
        }

    }
}
