﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ReviewMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Review']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Review')]";
        protected string checkbox = "//label[contains(text(),'Review of the request is complete')]";
        protected string submitButton = "//button[text()='Submit']";

        public ReviewMileStone(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON REVIEW DETAILS PAGE");
        }

        public ReviewMileStone ClickCheckBox()
        {
            GetElementByXpath(checkbox).Click();
            Console.WriteLine("-------------clicked on checkbox");
            return this;
        }

        public SubmissionMileStone ClickSubmitButton()
        {
            GetElementByXpath(submitButton).Click();
            Console.WriteLine("Click Submit button on Review Page");
            return new SubmissionMileStone(_driver);
        }
    }
}
