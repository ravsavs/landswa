﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class CaseDashboardPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Case Dashboard for Case Number')]");
        private IWebDriver _driver;
        protected string teamDashboardLink = "";
        //Tasks under Open Task
        protected string newTask = $"//*[text()='{taskName}']";
        protected string summaryTab = "//div[(text()='Summary')]";
        protected string taskStatus = "//div[(text()='Task Status')]";
        protected string relatedActionsTab = "//div[text()='Related Actions']";
        protected string lighteningIcon = "//span[@class = 'AccentText---color_accent']";
        protected string calculatePRLink = "//strong[text()='CALCULATE PRIORITY RATING']";
        protected string integrateWithObjLink = "//strong[text()='INTEGRATE WITH OBJECTIVE']";
        protected string integrateWithObjectiveButton = "//button[text()='INTEGRATE WITH OBJECTIVE']";
        protected string createTaskLink = "//strong[text() = 'CREATE TASK']";
        protected string addCaseNotesLink = "//strong[text()='ADD CASE NOTE']";
        protected string updateCaseDetailsLink = "//strong[text()='UPDATE CASE DETAILS']";
        protected string finaliseCaseLink = "//strong[text()='FINALISE CASE']";
        protected static string taskName = null;

        public CaseDashboardPage(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                _driver = driver;
            Console.WriteLine("ON CASE DASHBOARD PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public CaseDashboardPage ClickSummaryTab()
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            Console.WriteLine("-----------------Progress Bar disappeared from the page");
            try
            {
                GetElementByXpath(summaryTab).Click();
            }
            catch (UnhandledAlertException)
            {
                _driver.SwitchTo().Alert().Accept();

            }
            return this;

        }

        public BasePage ClickIntegratetWithObjectiveButton()
        {
            GetElementByXpath(integrateWithObjectiveButton).Click();
            Console.WriteLine("-----------------clicked on integrate with objective button from related actions");
            return new IntegrateWithObjectivePage(_driver);
        }

        public bool IsCaseIntegratedWithObjective(string fileNumber)
        {
            return GetElementByXpath($"//p[contains(text(),'{fileNumber}')]").Displayed;
        }

        public bool IsNewTaskPresentOnCaseDashboard(string taskName)
        {
            try
            {
                while (!_driver.PageSource.Contains(taskName))
                {
                    if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                        GetElementByXpath(summaryTab).Click();
                    Console.WriteLine("-----------------clicked on Summary Tab again for the task to appear on Case Dahboard: "+ taskName);
                }
            }
            catch (UnhandledAlertException e)
            {
                Console.WriteLine("-----------------Exception thrown while trying to click Summary tab...will try again");
                Console.WriteLine(e.Message);
                _driver.SwitchTo().Alert().Dismiss();
                IsNewTaskPresentOnCaseDashboard(taskName);
            }

            return GetElementByXpath($"//a[(text()='{taskName}')]").Displayed;
        }

        public BasePage ClickRelatedActionFromRelatedActionTab(RelatedActions relatedAction)
        {
            bool present = false;
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                GetElementByXpath(relatedActionsTab).Click();
            Console.WriteLine("-----------------clicked on Related Action button");

            if (IsPageLoadComplete()
                && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                do
                {
                    try
                    {
                        present = _driver.FindElement(By.XPath("//strong[text()='ADD CASE NOTE']")).Displayed;
                        Console.WriteLine("-----------------Is ADD CASE NOTE related action present on Page: "+ present);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Stale Element on Case Allocation Page " + e.Message);
                    }
                }
                while (!present && !_driver.PageSource.Contains("ADD CASE NOTE"));


            switch (relatedAction)
            {
                case RelatedActions.CalculatePriorityRating:
                    GetElementByXpath(calculatePRLink).Click();
                    Console.WriteLine("-----------------clicked on Priority Rating related action");
                    return new CalculatePriorityRatingPage(_driver);
                case RelatedActions.IntegrateWithObjective:
                    GetElementByXpath(integrateWithObjLink).Click();
                    Console.WriteLine("-----------------clicked on Integrate With Objective realted action");
                    return new IntegrateWithObjectivePage(_driver);
                case RelatedActions.CreateTask:
                    GetElementByXpath(createTaskLink).Click();
                    Console.WriteLine("-----------------clicked on Create Task related action");
                    return new CreateTaskPage(_driver);
                case RelatedActions.FinaliseCase:
                    GetElementByXpath(finaliseCaseLink).Click();
                    Console.WriteLine("-----------------clicked on Finalise Case related action");
                    return new FinaliseCasePage(_driver);
            }
            return this;
        }

        public BasePage ClickTaskInOpenTasksGrid(string taskName)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                GetElementByXpath($"//a[(text()='{taskName}')]").Click();
                Console.WriteLine("-----------------click on the task: " + taskName + " on Case Dashboard Page");

                switch (taskName)
                {
                    case "Allocate New Case":
                        return new AllocateNewCasePage(_driver);
                    case "Proceed With Case":
                        return new ProceedWithCasePage(_driver);
                    case "Assess Case Handover":
                        return new AssessCaseHandoverPage(_driver);
                    case "Approve or Amend Priority Rating":
                        return new ApproveOrAmendPriorityRatingPage(_driver);
                    case "Notification of Approved Priority Rating":
                        return new NotificationOfPriorityRatingPage(_driver);
                }
            }
            return this;

        }

        public void ClickFirstTaskInList()
        {
            if(HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                _driver.FindElement(By.XPath("(//tbody)[2]/tr/td[3]//p/a")).Click();
            Console.WriteLine("-----------------click the first task in the list on Case Dashboard");
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
        }
    }

}
