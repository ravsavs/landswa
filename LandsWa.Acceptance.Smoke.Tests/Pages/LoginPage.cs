﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Helper;


namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class LoginPage : BasePage
    {
        IWebDriver _driver = null;


        protected override By IsPageLoadedBy => By.XPath("//title[contains(text(),'Appian for WA Department of Lands (TEST)')]");
        public string Username = "//input[@id='un']";
        public string Password = "//input[@id='pw']";
        public string SubmitButton = "//input[@type='submit']";
        public IWebElement UserProfileImage => GetElementByXpath("//a[@aria-label='Open user options menu']");

        public LoginPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            if (env == Env.Test)
            {
                _driver.Navigate().GoToUrl(Constants.TestLoginUrl);
                Console.WriteLine("ON TEST LOGIN PAGE");
            }
            if (env == Env.PreProd)
            {
                _driver.Navigate().GoToUrl(Constants.PreprodLoginUrl);
                Console.WriteLine("ON PRE-PROD LOGIN PAGE");
            }
        }

        public MyDashboardPage LogIn(string username, string password)
        {
            bool flag = false;
            int i = 0;
            if (firstTimeLoggedIn && env != Env.PreProd)
            {
                GetElementByXpath("//a[.=' No']").Click();
                firstTimeLoggedIn = false;
            }
            GetElementByXpath(Username).Clear();
            GetElementByXpath(Username).SendKeys(username);
            Console.WriteLine("-----------------entered username: " + username);
            GetElementByXpath(Password).Clear();
            GetElementByXpath(Password).SendKeys(password);
            Console.WriteLine("-----------------entered password: "+ password);

            while (flag == false && i < 300)
            {
                try
                {
                    i++;
                    if (i == 1)
                    {
                        GetElementByXpath(SubmitButton).Click();
                        Console.WriteLine("-----------------clicked DONE button on Login Page");
                    }
                    flag = _driver.PageSource.Contains("My Dashboard - iWMS DoL Officer Site");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return new MyDashboardPage(_driver);
        }
        
        public MyDashboardPage SubmitNewCase(string login, string name, string password, User user, CaseType caseType, string applicantName, string category, string LGAName )
        {
            MyDashboardPage myDashboardPage = null;
            LoginPage loginPage = new LoginPage(_driver);

            var myDashboard = loginPage.LogIn(login, password);
            var page = (AssignApplicantCustomerPage)myDashboard.ClickCreateNewCaseButton()
                .InternalOrExternal(caseType)
                .ClickDoneButton();

            var applicantDetailsPage = page.SearchAnApplicantWithName(applicantName)
            .SelectTheApplicantFromSearchResultWithName(applicantName)
            .Continue();

            myDashboardPage = applicantDetailsPage
                 .ClickContinueButton().SelectGeneralRequestType()
                 .SelectCategoryFromDropdown(category)
                 .EnterDescription("Descr")
                 .ClickCLEFRequestCheckbox()
                 .ClickApplicantSignedCheckbox()
                 .EnterDateSigned()
                 .EnterDateReceived()
                 .UploadDocument("Document_1.txt")
                 .ClickContinueButton()
                 .AddLandRecordForLGA(LGAName)
                 .ClickContinueButton()
                 .ClickLGACheckboxToConsult()
                 .HasLGABeenConsultedRadioButtonResponse(Decision.Yes)
                 .ClickUpdateButton()
                 .ClickContinueButton()
                 .ClickContinueButton()
                 .ClickCheckBox()
                 .ClickSubmitButton()
                 .SendCaseSummaryToApplicantRadioButton(Decision.Yes)
                 .SelectMethodOfContact(ContactMethod.Email)
                 .AnyOtherDocumentsToSend(Decision.No)
                 .ClickReadyToEmailConfirmationCheckbox()
                 .ClickDoneButton();

            return myDashboardPage;
        }
    }
}
