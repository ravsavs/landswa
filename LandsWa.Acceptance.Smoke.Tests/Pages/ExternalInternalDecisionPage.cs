﻿using LandsWa.Acceptance.Smoke.Tests.Resources;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ExternalInternalDecisionPage : BasePage
    {
        private Dictionary<string, string> data;
        private IWebDriver _driver;
        private int timeout = 15;

        protected override By IsPageLoadedBy => By.XPath("//h2[contains(text(),'Create New Case')]]");

        string cancel = "//button[text()='Cancel']";
        string done = "//button[text()='Done']";
        string externallyRequested = "//label[text()='External']";
        string internallyRequested = "//label[text()='Internal']";
        string associatedCustomerYes = "//label[text()='Yes']";
        string associatedCustomerNo = "//label[text()='No']";
        string logo = "a[href='https://walandstest.appiancloud.com/suite/sites/officer-site']";
        string myDashboard = "a[href='https://walandstest.appiancloud.com/suite/sites/officer-site/page/my-dashboard']";
        private readonly string pageLoadedText = "Is it externally requested or internally initiated";
        private readonly string pageUrl = "/suite/sites/officer-site/page/my-dashboard";
        string teamDashboard = "a[href='https://walandstest.appiancloud.com/suite/sites/officer-site/page/team-dashboard']";

        public ExternalInternalDecisionPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        /// <summary>
        /// Click on Cancel Button.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage ClickCancelButton()
        {
            GetElementByXpath(cancel).Click();
            return this;
        }

        /// <summary>
        /// Click on Logo Link.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage ClickLogoLink()
        {
            GetElementByXpath(logo).Click();
            return this;
        }

        /// <summary>
        /// Click on My Dashboard Link.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage ClickMyDashboardLink()
        {
            GetElementByXpath(myDashboard).Click();
            return this;
        }

        /// <summary>
        /// Click on Team Dashboard Link.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage ClickTeamDashboardLink()
        {
            GetElementByXpath(teamDashboard).Click();
            return this;
        }

        public ExternalInternalDecisionPage InternalOrExternal(AppSubTestData dataRow)
        {
            switch (dataRow.CaseInitiation)
            {
                case "internalCase":
                    InternalOrExternal(CaseType.Internal);
                    if (dataRow.AssociatedCustomer.ToLower() == "yes")
                        IsCustomerAssociatedWithCase(CustomerAssociatedWithCase.Yes);
                    else if (dataRow.AssociatedCustomer.ToLower() == "no")
                        IsCustomerAssociatedWithCase(CustomerAssociatedWithCase.No);
                    break;
                case "externalCase":
                    InternalOrExternal(CaseType.External);
                    break;
                default:
                    return null;
            }
            return this;
        }

        /// <summary>
        /// Fill every fields in the page.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage InternalOrExternal(CaseType type)
        {
            caseType = type;
            switch (caseType)
            {
                case  CaseType.Internal:
                    GetElementByXpath(internallyRequested).Click();
                    break;
                case CaseType.External:
                    GetElementByXpath(externallyRequested).Click();
                    break;
                default:
                    return null;
            }
            return this;
        }

        ///<summary>
        ///Choose if the customer is associated with this INTERNAL case
        ///</summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage IsCustomerAssociatedWithCase(CustomerAssociatedWithCase decision)
        {
            customerAssociatedWithCase = decision;
            switch (customerAssociatedWithCase)
            {
                case CustomerAssociatedWithCase.Yes:
                    GetElementByXpath(associatedCustomerYes).Click();
                    break;
                case CustomerAssociatedWithCase.No:
                    GetElementByXpath(associatedCustomerNo).Click();
                    break;
                default:
                    return null;
            }
            return this;
        }


        /// <summary>
        /// Click on Done Button.
        /// </summary>
        /// <returns>The BasePage class instance.</returns>
        public BasePage ClickDoneButton()
        {
            GetElementByXpath(done).Click();
            if (caseType == CaseType.Internal)
                if (customerAssociatedWithCase == CustomerAssociatedWithCase.No)
                {
                    return new ApplicantDetailsMileStone(_driver);
                }
                else if (customerAssociatedWithCase == CustomerAssociatedWithCase.Yes)
                {
                    return new AssignApplicantCustomerPage(_driver);
                }

            if (caseType == CaseType.External)
            {
                return new AssignApplicantCustomerPage(_driver);
            }
            return null;
        }

        /// <summary>
        /// Verify that the page loaded completely.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage VerifyPageLoaded()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
            {
                return d.PageSource.Contains(pageLoadedText);
            });
            return this;
        }

        /// <summary>
        /// Verify that current page URL matches the expected URL.
        /// </summary>
        /// <returns>The ExternalInternalDecisionPage class instance.</returns>
        public ExternalInternalDecisionPage VerifyPageUrl()
        {
            new WebDriverWait(_driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
            {
                return d.Url.Contains(pageUrl);
            });
            return this;
        }
    }
}
