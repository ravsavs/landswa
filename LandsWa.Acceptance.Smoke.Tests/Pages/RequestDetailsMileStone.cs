﻿using OpenQA.Selenium;
using System;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using LandsWa.Acceptance.Smoke.Tests.Resources;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class RequestDetailsMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Request Details']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Request Details')]";
        protected string continueButton = "//button[text()='Continue']";
        protected string generalRequestType = "//label[text()='General request']";
        protected string eventRequestType = "//label[text()='Request for access to Crown land for an event']";
        protected string LgaType = "//label[text()='Request from Local Government, Management Body or State Government Agency']";

        protected string categoryDropdown = "//div[text()='Select one item...']";
        protected string descriptionTextArea = "(//textarea)[1]";
        protected string caseDescriptionTextArea = "(//textarea)[2]";
        protected string CLEFRequestCheckbox = "//label[text()=' Request received on a CLEF']";
        protected string signedCheckbox = "//label[text()='Signed']";
        protected string projectNameTextBox = "//label[text()='Project Name']/../../div[2]//input";
        protected string caseDueDate = "//label[text()='Case Due Date']/../../div[2]//input";
        protected string dateReceived = "//label[text()='Date Received']/../../div[2]//input";
        protected string dateSigned = "//label[text()='Date Signed']/../../div[2]//input";
        protected string uploadButton = "//button[text()='Upload']";
        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string positionTextBox = "//strong[text()='Position']/../../../../../../div[2]//input";
        // Event Request Type Inputs
        protected string eventFromDate = "//span[text()='From']/../../div[2]//input[contains(@class,'DatePickerWidget')]";
        protected string eventFromTime = "//span[text()='From']/../../div[2]//input[contains(@class,'TimeWidget')]";
        protected string eventToDate = "//span[text()='To']/../../div[2]//input[contains(@class,'DatePickerWidget')]";
        protected string eventToTime = "//span[text()='To']/../../div[2]//input[contains(@class,'TimeWidget')]";
        protected string eventPurpose = "//textarea";
        protected string eventLandSize = "//label[text()='Approximate size of the land area required for the event']/../../div[2]/div/input";
        protected string eventUploadLayoutDocument = "//label[text()='Upload event layout']/../../div[2]//input";
        protected string eventExternalStructuresYes = "//input[@value='Yes']";
        protected string eventExternalStructuresNo = "//input[@value='No']";
        protected string eventVisitors = "//*[contains(text(),'Visitor')]/../../td[2]/div/input";
        protected string eventEmployees = "//*[contains(text(),'Employees')]/../../td[2]/div/input";
        protected string eventArtists = "//*[contains(text(),'Artists')]/../../td[2]/div/input";
        protected string eventRevenue = "//*[contains(text(),'Revenue')]/../../td[2]/div/input";
        protected string eventGrants = "//*[contains(text(),'Grants')]/../../td[2]/div/input";
        protected string eventExpenditure = "//*[contains(text(),'Expenditure')]/../../td[2]/div/input";
        protected string eventApplicationDateReceived = "//label[text()='Date Received']/../../div[2]//input[contains(@class,'DatePickerWidget')]";
        protected string eventUploadApplicantsRequest = "//strong[text()='Date Received *']/../../../../../../../../../div[6]//input";
        protected string eventUploadGrantDocument = "//label[text()='Upload documentation regarding the grant/sponsorship received for the event']/../../div[2]//input";

        protected string currentDate = DateTime.Now.ToString("dd/MM/yyyy");
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";
        protected bool ApplicantSignedTheApp = false;

        public RequestDetailsMileStone(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
        }

        public RequestDetailsMileStone SelectGeneralRequestType()
        {
            GetElementByXpath(generalRequestType).Click();
            return this;
        }

        public RequestDetailsMileStone SelectLgaRequestType()
        {
            GetElementByXpath(LgaType).Click();
            return this;
        }

        public RequestDetailsMileStone SelectEventRequestType()
        {
            GetElementByXpath(eventRequestType).Click();
            return this;
        }

        public RequestDetailsMileStone CompleteEventDetails(AppSubTestData dataRow)
        {
            GetElementByXpath(eventFromDate).SendKeys(dataRow.EventFromDate);
            GetElementByXpath(eventFromTime).SendKeys(dataRow.EventFromTime);
            GetElementByXpath(eventToDate).SendKeys(dataRow.EventToDate);
            GetElementByXpath(eventToTime).SendKeys(dataRow.EventToTime);
            GetElementByXpath(eventPurpose).SendKeys(dataRow.EventPurpose);
            GetElementByXpath(eventLandSize).SendKeys(dataRow.EventLandSize);
            UploadDocument(_driver.FindElement(By.XPath(eventUploadLayoutDocument)), dataRow.EvenLayoutDocument);
            GetElementByXpath(eventVisitors).SendKeys(dataRow.NoOfVisitors);
            GetElementByXpath(eventEmployees).SendKeys(dataRow.NoOfEmployees);
            GetElementByXpath(eventArtists).SendKeys(dataRow.NoOfArtists);
            GetElementByXpath(eventRevenue).SendKeys(dataRow.Revenue);
            GetElementByXpath(eventGrants).SendKeys(dataRow.Grants);
            GetElementByXpath(eventExpenditure).SendKeys(dataRow.Expenses);
            GetElementByXpath(eventApplicationDateReceived).SendKeys(dataRow.DateReceived); 
            GetElementByXpath(staticPageElement).Click();

            if(dataRow.Grants != "" &&  HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                UploadDocument(_driver.FindElement(By.XPath(eventUploadGrantDocument)), dataRow.GrantDocumentName);

            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                UploadDocument(_driver.FindElement(By.XPath(eventUploadApplicantsRequest)), dataRow.ApplicantRequestDocumentName);

            return this;
        }

        public RequestDetailsMileStone CompleteEventDetails()
        {
            GetElementByXpath(eventFromDate).SendKeys(currentDate);
            GetElementByXpath(eventFromTime).SendKeys("10:00");
            GetElementByXpath(eventToDate).SendKeys(currentDate);
            GetElementByXpath(eventToTime).SendKeys("23:35");
            GetElementByXpath(eventPurpose).SendKeys("Splendour in the Grass");
            GetElementByXpath(eventLandSize).SendKeys("24596");
            UploadDocument(_driver.FindElement(By.XPath(eventUploadLayoutDocument)), "20180619 Event Layout or map.docx");
            GetElementByXpath(eventVisitors).SendKeys("24500");
            GetElementByXpath(eventEmployees).SendKeys("75");
            GetElementByXpath(eventArtists).SendKeys("14");
            GetElementByXpath(eventRevenue).SendKeys("750000");
            GetElementByXpath(eventGrants).SendKeys("175250");
            GetElementByXpath(eventExpenditure).SendKeys("458769");
            GetElementByXpath(eventApplicationDateReceived).SendKeys(currentDate);
            GetElementByXpath(staticPageElement).Click();
            UploadDocument(_driver.FindElement(By.XPath(eventUploadApplicantsRequest)), "20180611 Applicants Request.docx");
            UploadDocument(_driver.FindElement(By.XPath(eventUploadGrantDocument)), "20180619 Event Grant or Sponsorship Details.docx");

            return this;
        }

        public RequestDetailsMileStone SelectRequestType(RequestType type)
        {
            switch (type)
            {
                case RequestType.General:
                    GetElementByXpath(generalRequestType).Click();
                    break;
                case RequestType.LGA:
                    GetElementByXpath(LgaType).Click();
                    break;
                case RequestType.Event:
                    GetElementByXpath(eventRequestType).Click();
                    break;
            }
            return this;
        }

        public RequestDetailsMileStone EnterPosition(string position)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(positionTextBox).SendKeys(position);
            return this;
        }

        public RequestDetailsMileStone EnterProject(string projectName)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(projectNameTextBox).SendKeys(projectName);
            return this;
        }

        public RequestDetailsMileStone EnterCaseDueDate(string currentDate)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(caseDueDate).SendKeys(currentDate);
            return this;
        }

        public RequestDetailsMileStone SelectCategoryFromDropdown(string name)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(categoryDropdown).Click();
            GetElementByXpath($"//*[text()='{name}']").Click();
            return this;
        }

        public RequestDetailsMileStone EnterDescription(string desc)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(descriptionTextArea).SendKeys(desc);
            return this;
        }

        public RequestDetailsMileStone EnterCaseDescription(string desc)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(caseDescriptionTextArea).SendKeys(desc);
            return this;
        }

        public RequestDetailsMileStone ClickCLEFRequestCheckbox(string decision)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            if (decision.ToLower() == "yes")
                ClickCLEFRequestCheckbox();
            return this;
        }

        public RequestDetailsMileStone ClickCLEFRequestCheckbox()
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(CLEFRequestCheckbox).Click();
            return this;
        }

        public RequestDetailsMileStone ClickApplicantSignedCheckbox(string decision)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            if (decision.ToLower() == "yes")
            {
                ApplicantSignedTheApp = true;
                ClickApplicantSignedCheckbox();
            }
            return this;
        }

        public RequestDetailsMileStone ClickApplicantSignedCheckbox()
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(signedCheckbox).Click();
            return this;
        }

        public RequestDetailsMileStone EnterDateSigned(string currentDate)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            if (ApplicantSignedTheApp)
            {
                if (currentDate == "")
                    EnterDateSigned();
                else
                {
                    var ele = GetElementByXpath(dateSigned);
                    ele.SendKeys(currentDate);
                    GetElementByXpath(staticPageElement).Click();
                }
            }
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            return this;
        }

        public RequestDetailsMileStone EnterDateSigned()
        {
            var ele = GetElementByXpath(dateSigned);
            ele.SendKeys(currentDate);
            GetElementByXpath(staticPageElement).Click();
            return this;
        }

        public RequestDetailsMileStone EnterDateReceived(string currentDate)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(dateReceived).SendKeys(currentDate);
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            return this;
        }

        public RequestDetailsMileStone EnterDateReceived()
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(dateReceived).SendKeys(currentDate);
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            return this;
        }

        public RequestDetailsMileStone UploadDocument(string fileName)
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(staticPageElement).Click();
            UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            return this;
        }


        public LandDetailsMileStone ClickContinueButton()
        {
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            GetElementByXpath(continueButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Request Details page");
            }
            catch (Exception e) { Console.WriteLine("Request Details Page has no validation errors"); }
            Console.WriteLine("Moved past Request Details Page");
            HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear);
            return new LandDetailsMileStone(_driver);
        }
    }
}
