﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class LandDetailsMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Land Details']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Land Details')]";

        protected string generalInfotextArea = "//textarea";

        protected string addLandRecordImg = "//img[@aria-label='Add Land Record']";
        protected string landRecordAssociatedWithInternalCase = "//label[text()='Yes']";
        protected string landRecordNotAssociatedWithInternalCase = "//label[text()='No']";
        protected string lotNumberField = "//label[text()='Lot Number']/../../div[2]//input";
        protected string planNumberField = "//label[text()='Plan Number']/../../div[2]//input";
        protected string pinField = "//label[text()='PIN']/../../div[2]//input";
        protected string reserveNumberField = "//label[text()='Reserve Number(if applicable)']/../../div[2]//input";
        protected string reserveClassDropDown = "//span[text()='Reserve Class(if applicable and known)']/../../div[2]/div/div";
        //do it dynamic
        protected string reserveClassOption = "//*[text()='CLASS A']";

        protected string reservePurpose = "//*[text()='Reserve Purpose(if applicable and known)']/../../div[2]//input";
        protected string landTitleIsAvailable = "//span[text()='Is a Land Title Available?']/../../div[2]//label[text()='Yes']";
        protected string volumeField = "//label[text()='Volume']/../../div[2]//input";
        protected string FolioField = "//label[text()='Folio']/../../div[2]//input";
        protected string uploadMapField = "//label[text()='Upload maps of the area/s of interest']/../../div[2]//input";
        protected string uploadLandTitle = "//label[text()='Upload Land Title']/../../div[2]//input";
        protected string PIHName = "//label[text()='Primary Interest Holder/Proprietor Name']/../../div[2]//input";
        protected string landTitleIsNotAvailable = "//span[text()='Is a Land Title Available?']/../..//label[text()='No']";
        protected string streetNumberField = "//label[text()='Street Number']/../../div[2]//input";
        protected string streetNameField = "//label[text()='Street Name']/../../div[2]//input";
        protected string streetTypeDropDown = "//span[text()='Street Type']/../../div[2]/div/div";
        //do it dynamic
        protected string streetTypeOption = "//*[text()='APP']";

        protected string suburbField = "//span[text()='Suburb']/../..//input";
        protected string landRecordCommentTextarea = "//label[text()='Land Record Comment']/../../div[2]//textarea";

        protected string localGovtAuthority = "//span[text()='Local Government Authority']/../..//input";
        protected string selectedLGA = "(//span[@class='PickerTokenWidget---label'])[2]";
        protected string addButton = "//button[text()='ADD']";
        protected string continueButton = "//button[text()='Continue']";
        protected string selectedLGATableCell = "//tbody/tr/td[9]";

        public static string SuburbName { get; set; } = "";

        public LandDetailsMileStone(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
        }

        public LandDetailsMileStone AreLandRecordsAssociatedWithThisInternalCase(IsLandRecordAssociatedWithThisInternalCase decision)
        {
            isLandRecordAssociatedWithThisInternalCase = decision;
            switch (isLandRecordAssociatedWithThisInternalCase)
            {
                case (IsLandRecordAssociatedWithThisInternalCase.Yes):
                    GetElementByXpath(landRecordAssociatedWithInternalCase).Click();
                    break;
                case (IsLandRecordAssociatedWithThisInternalCase.No):
                    GetElementByXpath(landRecordNotAssociatedWithInternalCase).Click();
                    break;
                default:
                    return this;
            }
            return this;
        }

        public LandDetailsMileStone EnterGeneralInformation(string comment)
        {
            GetElementByXpath(generalInfotextArea).SendKeys(comment);
            return this;
        }

        public LandDetailsMileStone UploadMapDocument(string fileName)
        {
            if (fileName != "")
            {
                GetElementByXpath(staticPageElement).Click();
                UploadDocument(_driver.FindElement(By.XPath(uploadMapField)), fileName);
            }
            return this;
        }

        public LandDetailsMileStone AddALandRecord(string lotNumber, string planNumber,
            string pin, string reserveNumber, string reserveClass, string reservePurpose,
            Decision islandTitleAvailable, string streetNum, string streetName, string streetType,
            string suburb, string lga, string comments, string volume, string folio, string fileName,
            string pihName,string isApplicantPIH)
        {
            while(!HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear));
            {
                Thread.Sleep(1000);
                ClickOnAddLandRecordImage();
                Console.WriteLine("-----------Clicked on + on Land Details Page-----------------");
            }
            GetElementByXpath(lotNumberField).SendKeys(lotNumber);
            _driver.FindElement(By.XPath(planNumberField)).SendKeys(planNumber);
            _driver.FindElement(By.XPath(pinField)).SendKeys(pin);
            _driver.FindElement(By.XPath(reserveNumberField)).SendKeys(reserveNumber);
            if (reserveClass != "")
            {
                _driver.FindElement(By.XPath(reserveClassDropDown)).Click();
                GetElementByXpath($"//*[text()='{reserveClass}']").Click();
            }
            GetElementByXpath(this.reservePurpose).SendKeys(reservePurpose);
            if (islandTitleAvailable == Decision.Yes)
            {
                BasePage.IsLandTitleAvailable = true;
                GetElementByXpath(landTitleIsAvailable).Click();
                GetElementByXpath(volumeField).SendKeys(volume);
                GetElementByXpath(FolioField).SendKeys(folio);
                GetElementByXpath(staticPageElement).Click();

                if (fileName != "" && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                    UploadDocument(_driver.FindElement(By.XPath(uploadLandTitle)), fileName);
                Console.WriteLine("-----------Upload Land Title file on Land Details Page-----------------");

                if (pihName != "")
                {
                    GetElementByXpath(PIHName).SendKeys(pihName);
                    BasePage.PIHExists = true;
                }
                GetElementByXpath($"//span[text()='Is the Applicant/Customer the Primary Interest Holder/Proprietor']/../../div[2]//label[text()='{isApplicantPIH}']").Click();
            }
            if (islandTitleAvailable == Decision.No)
                _driver.FindElement(By.XPath(landTitleIsNotAvailable)).Click();

            _driver.FindElement(By.XPath(streetNumberField)).SendKeys(streetNum);
            _driver.FindElement(By.XPath(streetNameField)).SendKeys(streetName);

            if (streetType != "" && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                GetElementByXpath(streetTypeDropDown).Click();
                _driver.FindElement(By.XPath($"//*[text()='{streetType}']")).Click();
            }

            ScrollToView(GetElementByXpath(suburbField));

            if( suburb != "" && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                SelectOptionFromAutoSuggestionList(suburbField, suburb);
            Console.WriteLine("-----------Suburb added on Land Details Page-----------------");

            ScrollToView(GetElementByXpath(localGovtAuthority));

            if (lga != "" && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                SelectOptionFromAutoSuggestionList(localGovtAuthority, lga);
            Console.WriteLine("-----------LGA added on Land Details Page-----------------");

            ScrollToView(GetElementByXpath(landRecordCommentTextarea));

            if (suburb!="" && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                LGAName = GetElementByXpath("(//span[@class='PickerTokenWidget---label'])[2]").Text;
            else if(HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                LGAName = GetElementByXpath("(//span[@class='PickerTokenWidget---label'])[1]").Text;

            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                _driver.FindElement(By.XPath(landRecordCommentTextarea)).SendKeys(comments);

            Console.WriteLine("-----------Entered General Comments on Land Details Page-----------------");

            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                ClickAddButton();
            Console.WriteLine("-----------Clicked on ADD button on Land Details Page-----------------");

            return this;
        }

        private void SelectOptionFromAutoSuggestionList(string xpath, string option)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                var ele = GetElementByXpath(xpath);
                ele.SendKeys(option);
                ele = GetElementByXpath("//ul[@role='listbox']//p[1]");
                try
                {
                    while (ele.Text == "Searching…")
                        ele = GetElementByXpath("//ul[@role='listbox']//p[1]");
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Searching drop down option just vanished. Scripts performing correctly. {e.Message}");
                }

                GetElementByXpath("(//ul[@role='listbox']//p)[1]").Click();
                Console.WriteLine($"----------- {option} added from the drop down list-----------------");
                //ele.SendKeys(Keys.ArrowDown);
                //Thread.Sleep(500);
                //ele.SendKeys(Keys.Enter);
            }
        }

        public LandDetailsMileStone AddLandRecordForLGA(string LGAName)
        {
            if (isLandRecordAssociatedWithThisInternalCase == IsLandRecordAssociatedWithThisInternalCase.Yes)
            {
                ClickOnAddLandRecordImage();
                AddLGA(LGAName);
                ClickAddButton();
            }
            return this;
        }

        public LandDetailsMileStone ClickOnAddLandRecordImage()
        {
            GetElementByXpath(addLandRecordImg).Click();
            return this;
        }

        public LandDetailsMileStone AddLGA(string lga)
        {
            var ele = GetElementByXpath(localGovtAuthority);
            ele.SendKeys(lga);
            var dropDown = _driver.FindElement(By.XPath("(//ul[@role='listbox']//p)[1]"));
            while (dropDown.Text == "Searching…") ;
            ele.SendKeys(Keys.ArrowDown);
            ele.SendKeys(Keys.Enter);
            if (SuburbName == "")
                LGAName = GetElementByXpath("(//span[@class='PickerTokenWidget---label'])[1]").Text;
            else
                LGAName = GetElementByXpath("(//span[@class='PickerTokenWidget---label'])[2]").Text;
            return this;
        }

        public LandDetailsMileStone ClickAddButton()
        {
            GetElementByXpath(addButton).Click();
            Console.WriteLine("------------------------Clicked Add button on Land Details Page");
            return this;
        }

        public bool IsLandRecordAdded()
        {
            return GetElementByXpath(selectedLGATableCell).Text == LGAName;
        }

        public ConsultationMileStone ClickContinueButton()
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                GetElementByXpath(continueButton).Click();
                Console.WriteLine("------------------------Clicked Continue button on Land Details Page");
                return new ConsultationMileStone(_driver);
            }
            else
                return null;
        }
    }
}
