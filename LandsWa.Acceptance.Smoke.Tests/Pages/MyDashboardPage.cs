﻿using OpenQA.Selenium;
using System;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class MyDashboardPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//title[contains(text(),'My Dashboard - iWMS DoL Officer Site')]");
        private IWebDriver _driver;
        protected string teamDashboardLink = "//div[text()='Team Dashboard']";
        protected string filtersExpand = "//a[contains(text(),'Filters')]";
        protected string caseNumberFilterTextBox = "//*[text()='Case Number']/../../div[2]/div/input";
        protected string applyFilterButton = "//button[contains(text(),'Apply')]";
        protected string caseNumber = null;
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public MyDashboardPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            Console.WriteLine("ON MY DASHBOARD PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsOfficerNameDisplayed(string name)
        {
            Console.WriteLine("-----------------searching for User Name: " + name + " on My Dashboard");
            var NameElement = GetElementByXpath($"//strong[contains(text(),'{name}')]");
            Console.WriteLine("-----------------user name is displayed");
            return NameElement.Displayed;
        }

        public bool IsCaseFoundInMyDashboard(string caseNumber)
        {
            var caseNumberElement = GetElementByXpath($"//a[.='{caseNumber}']");
            return caseNumberElement.Displayed;
        }

        internal ExternalInternalDecisionPage ClickCreateNewCaseButton()
        {
            IWebElement ele = null;
            try
            {
                ele = GetElementByXpath("//button[contains(text(),'CREATE NEW CASE')]");
            }
            catch(Exception e)
            {
                Console.WriteLine("'CREATE NEW CASE' button is not available to this user");
                Console.WriteLine(e.InnerException);
            }
            ele.Click();
            Console.WriteLine("-----------------Clicked on Create New Case Button");
            return new ExternalInternalDecisionPage(_driver);
        }

        public MyDashboardPage FilterAndSearchByCaseNumber(string caseNumber)
        {
            bool displayed = false;
            int i = 0;
            GetElementByXpath(filtersExpand).Click();
            Console.WriteLine("-----------------Clicked on Filters to expand filters");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                GetElementByXpath(caseNumberFilterTextBox).Clear();
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                GetElementByXpath(caseNumberFilterTextBox).SendKeys(caseNumber);
                Console.WriteLine("-----------------Entered the Case Number for search " + caseNumber);
            }
            GetElementByXpath(applyFilterButton).Click();
            Console.WriteLine("-----------------Clicked the Apply button to search for the case number");

            while ((!HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear)
                || !displayed) && i < 10)
            {
                try
                {
                    i++;
                    Console.WriteLine("-----------------waiting for the case number to appear in search results");
                    displayed = _driver.FindElement(By.XPath($"//a[.='{caseNumber}']")).Displayed;
                    Console.WriteLine("-----------------case number displayed: " + displayed);
                    GetElementByXpath(applyFilterButton).Click();
                    Console.WriteLine("-----------------click Apply button again");
                }
                catch (Exception e)
                {
                    Console.WriteLine("-----------------Searching for the case number threw an exception");
                    Console.WriteLine(e.Message);
                }
            }
            return this;
        }

        public CaseDashboardPage ClickCaseNumberTaskGridLink(string caseNumber)
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath($"//a[.='{caseNumber}']")));
            GetElementByXpath($"//a[.='{caseNumber}']").Click();
            Console.WriteLine("-----------------clicked on the case number in the search results");
            return new CaseDashboardPage(_driver);
        }

        public MyDashboardPage ClickMYDashboardLink()
        {
            GetElementByXpath("//div[text()='My Dashboard']").Click();
            Console.WriteLine("-----------------clicked on My Dashboard link");
            return this;
        }

        public TeamDashboardPage NavigateToTeamDashboard()
        {
            GetElementByXpath(teamDashboardLink).Click();
            Console.WriteLine("-----------------clicked on Team Dashboard link");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new TeamDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
    }
}
