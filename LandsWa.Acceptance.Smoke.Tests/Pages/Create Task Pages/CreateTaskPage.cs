﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class CreateTaskPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[text()='Create Task']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[text()='Create Task']";
        protected string officerField = "//span[text()='Officer *']/../../../../div[2]//input";
        protected string searchedOfficer = "//span[text()='1 result available. Use enter to select.']";
        protected string identifyWIField = "//span[text()='Identify Work Instruction *']/../../../../div[2]//input";
        protected string searchedWI = "//span[text()='1 result available. Use enter to select.']";
        protected string taskNameDropDown = "//div[@role='listbox']";
        public static string taskName;
        protected string searchedTaskName = $"//span[contains(text(),'{taskName}')]";
        protected string commentsTextBox = "//textarea";
        protected string dueDateField = "//label[text()='Due Date']/../../div[2]//input";
        protected string noUploadOption = "//label[text()='No']";
        protected string yesUploadOption = "//label[text()='Yes']";
        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string uploadButton = "//button[contains(text(),'Upload')]";
        protected string tenureSearchItem3 = "//label[contains(text(),'Identify Native Title interests on all land records')]";
        protected string tenureSearchItem4 = "//label[contains(text(),'Identify Mining, Petroleum and Geothermal interests on all land records')]";
        protected string tenureSearchItem5 = "//label[contains(text(),'Perform corporate entity search')]";
        protected string doneButton = "//button[contains(text(),'DONE')]";
        protected string cancelButton = "//button[contains(text(),'CANCEL')]";    
        protected string currentDate = DateTime.Now.ToString("dd/MM/yyyy");
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public CreateTaskPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public CreateTaskPage EnterOfficerName(string officerName)
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(officerField)));
            var element = GetElementByXpath(officerField);
            element.SendKeys(officerName);
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(searchedOfficer)));
            element.SendKeys(Keys.Enter);
            return this;
        }

        public CreateTaskPage EnterWI(string workInstruction)
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(identifyWIField)));
            var element = GetElementByXpath(identifyWIField);
            element.SendKeys(workInstruction);
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(searchedWI)));
            element.SendKeys(Keys.Enter);
            return this;
        }

        public CreateTaskPage EnterTaskName(string taskName)
        {
            var element = GetElementByXpath(taskNameDropDown);
            element.Click();
            element.SendKeys(taskName);
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath($"//span[contains(text(),'{taskName}')]")));
            element.SendKeys(Keys.Enter);
            if(taskName.Equals("Tenure Search"))
            {
                GetElementByXpath(tenureSearchItem3).Click();
                GetElementByXpath(tenureSearchItem4).Click();
                GetElementByXpath(tenureSearchItem5).Click();
            }
            return this;
        }

        public CreateTaskPage EnterComments(string comments)
        {
            GetElementByXpath(commentsTextBox).SendKeys(comments);
            return this;
        }

        public CreateTaskPage SelectNoUploadOption()
        {
            GetElementByXpath(noUploadOption).Click();
            return this;
        }

        public CreateTaskPage SelectYesUploadOptionAndUploadFile(string fileName)
        {            
            GetElementByXpath(yesUploadOption).Click();
            ScrollToView(GetElementByXpath(uploadButton));
            UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            if (IsPageLoaded())
            {
                GetElementByXpath(doneButton).Click();
            }

            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }

            Console.WriteLine("Completed Create Task");

            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new CaseDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
            
    }
}
