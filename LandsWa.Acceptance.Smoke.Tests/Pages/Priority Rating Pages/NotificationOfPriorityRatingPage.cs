﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    class NotificationOfPriorityRatingPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Notification of Approved Case Priority Rating')]");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[contains(text(),'Notification of Approved Case Priority Rating')]";
        protected string OKButton = "//button[text()='OK']";


        public NotificationOfPriorityRatingPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON PRIORITY RATING NOTIFICATION PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsNotificationTaskForCase(string caseNumber)
        {
            return GetElementByXpath($"//a[.='{caseNumber}']").Displayed;
        }

        public CaseDashboardPage ClickOKButton()
        {
            GetElementByXpath(OKButton).Click();
            Console.WriteLine("-----------------clicked ON button on priority rating notification");
            return new CaseDashboardPage(_driver);
        }
    }
}
