﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class CalculatePriorityRatingPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[text()='Calculate Priority Rating']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[text()='Calculate Priority Rating']";
        protected string calculatePRPageTitle = "//h1[text()='Calculate Priority Rating']";
        protected string workInstructionField = "//input[@aria-autocomplete='list']";
        //static string optionText = null;
        //protected string  priorityOptionLabel = $"//label[contains(text(),'{optionText}')]";
        protected string cancelButton = "//button[text()='Cancel']";
        protected string doneButton = "//button[text()='Done']";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";


        public CalculatePriorityRatingPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON CALCULATE PRIORITY RATING PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public CalculatePriorityRatingPage EnterWI(string workInstruction)
        {
            EnterWIPredictiveField(workInstructionField, workInstruction);
            Console.WriteLine("-----------------entered work instruction");
            return this;
        }

        public CalculatePriorityRatingPage SelectPriorityOption(string optionText)
        {
            GetElementByXpath($"//label[contains(text(),'{optionText}')]").Click();
            Console.WriteLine("-----------------slected priority rating option: " +optionText);
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }

            Console.WriteLine("-----------------clicked DONE button; Completed Calculate Priority Rating");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new CaseDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
    }
}
