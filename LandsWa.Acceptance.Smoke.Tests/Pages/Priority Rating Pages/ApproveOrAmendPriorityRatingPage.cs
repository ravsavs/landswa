﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ApproveOrAmendPriorityRatingPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Approve or Amend the Case Priority Rating')]");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[contains(text(),'Approve or Amend the Case Priority Rating')]";
        protected string acceptButton = "//button[contains(text(),'Accept')]";
        protected string decisionDropDown = "//div[@class='ContentLayout---content_layout']//div[text()='--- Select a value ---']";
        protected string commentsTextBox = "//textarea";
        protected string editButton = "//button[contains(text(),'EDIT')]";
        protected string updateButton = "//button[contains(text(),'UPDATE')]";
        protected string doneButton = "//button[contains(text(),'Done')]";
        protected string cancelButton = "//button[contains(text(),'Cancel')]";
        protected string saveExitButton = "//button[contains(text(),'Save & Exit')]";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public ApproveOrAmendPriorityRatingPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON APPROVE OR AMEND PRIORITY RATING PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsPriorityApprovalTaskForCaseDisplayed(string caseNumber)
        {
            return GetElementByXpath($"//a[.='{caseNumber}']").Displayed;
        }

        public ApproveOrAmendPriorityRatingPage ClickAcceptTask()
        {
            Thread.Sleep(500);
            GetElementByXpath(acceptButton).Click();
            Console.WriteLine("-----------------clicked Accept button for the task");
            return this;
        }

        public ApproveOrAmendPriorityRatingPage SelectPriorityRatingDecision(string priorityRatingDecision)
        {
            if (IsPageLoadComplete()
                && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear)
                && priorityRatingDecision != "")
                do
                {
                    try
                    {
                        _driver.FindElement(By.XPath(decisionDropDown)).Click();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Stale Element on Case Allocation Page " + e.Message);
                    }
                }
                while (!_driver.PageSource.Contains("Amend and Approve Priority Rating"));

            if (priorityRatingDecision.ToLower().Contains("approve"))
            {
                GetElementByXpath($"//*[text()='Approve Priority Rating']").Click();
                Console.WriteLine("-----------------chose Approve Priority Rating from the dropdown");
            }
            else if (priorityRatingDecision.ToLower().Contains("amend"))
            {
                GetElementByXpath($"//*[text()='Amend and Approve Priority Rating']").Click();
                Console.WriteLine("-----------------chose Amend and Approve Priority Rating from the dropdown");
            }
            return this;
        }

        public ApproveOrAmendPriorityRatingPage EnterManagerComments(string comments)
        {
            GetElementByXpath(commentsTextBox).SendKeys(comments);
            Console.WriteLine("-----------------entered manager's comment: "+ comments);
            return this;
        }

        public ApproveOrAmendPriorityRatingPage ClickEditButton()
        {
            GetElementByXpath(editButton).Click();
            Console.WriteLine("-----------------clicked edit button");
            return this;
        }

        public ApproveOrAmendPriorityRatingPage EditPriorityRating(string managerPriorityOption)
        {
            ClickEditButton();
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(updateButton)));
                Console.WriteLine("-----------------clicked edit button; Update button is visible");
                GetElementByXpath($"//label[contains(text(),'{managerPriorityOption}')]").Click();
                Console.WriteLine("-----------------clicked Manager Priority option: " + managerPriorityOption);
                if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                {
                    GetElementByXpath(updateButton).Click();
                    Console.WriteLine("-----------------clicked update button");
                }
                if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                    Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(editButton)));
            }
            return this;
        }

        public CaseDashboardPage ClickSaveAndExitButton()
        {
            ScrollToView(_driver.FindElement(By.XPath(saveExitButton)));
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(saveExitButton)));
            _driver.FindElement(By.XPath(saveExitButton)).Click();
            Console.WriteLine("-----------------clicked on Save and Exit button");
            return new CaseDashboardPage(_driver);
        }

        public CaseDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }

            Console.WriteLine("-----------------clicked DONE button; Completed Amend or Approve Priority Rating");

            return new CaseDashboardPage(_driver);
        }


    }
}
