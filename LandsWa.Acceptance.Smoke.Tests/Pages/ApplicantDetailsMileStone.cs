﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ApplicantDetailsMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Applicant Details']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2/a[contains(text(), 'Applicant Details')]";
        protected string continueButton = "//button[text()='Continue']";
        protected string uploadButton = "//button[text()='Upload']";
        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string heading = "//h2/a";

        public ApplicantDetailsMileStone(IWebDriver driver) : base(driver)
        {
            CaseNumber = "NOT ASSIGNED YET";
            _driver = driver;
            GetElementByXpath(staticPageElement);
            CaseNumber = GetElementByXpath(heading).Text;
            CaseNumber = System.Text.RegularExpressions.Regex.Match(CaseNumber, @"\d+").Value;
            Console.WriteLine("ON APPLICANT DETAILS PAGE");
            Console.WriteLine("-----------------Case Number for the case: "+ CaseNumber);
        }

        public ApplicantDetailsMileStone UploadDocument(string fileName)
        {
            GetElementByXpath(staticPageElement).Click();
            UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
            Console.WriteLine("-----------------uploaded a document on Applicant Details page: "+fileName);
            return this;
        }

        public RequestDetailsMileStone ClickContinueButton()
        {
            GetElementByXpath(continueButton).Click();
            Console.WriteLine("-----------------clicked Continue button on Applicant Details page");
            return new RequestDetailsMileStone(_driver);
        }
    }
}
