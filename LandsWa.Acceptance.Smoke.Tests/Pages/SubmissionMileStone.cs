﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class SubmissionMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Submission']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Submission Receipt')]";
        protected string methodOfContactDropdown = "//span[text()='Method of contact *']/../../../../div[2]/div/div[2]/div/div";
        protected string copyEmailToCustomer = "//label[text()='Copy email to Customer']";
        protected string copyEmailToOtherRecipients = "//label[text()='Copy email to Other Recipients']";
        protected string copyEmailToOtherRecipientsTextBox = "//label[text()='Copy email to Other Recipients *']/../../../../../../../div[11]//input";
        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string printAndPostDocumentSentDate = "//input[contains(@class,'DatePicker')]";
        protected string readyToEmailCheckbox = "//label[text()='* Confirm ready to email']";
        protected string doneButton = "//button[text()='DONE']";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";
        protected ContactMethod contactMethod;

        public SubmissionMileStone(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON CASE SUBMISSION PAGE");
        }

        public SubmissionMileStone SendCaseSummaryToApplicantRadioButton(string decision)
        {
            switch (decision.ToLower())
            {
                case "yes":
                    SendCaseSummaryToApplicantRadioButton(Decision.Yes);
                    break;
                case "no":
                    SendCaseSummaryToApplicantRadioButton(Decision.No);
                    break;
            }
            return this;
        }

        public SubmissionMileStone SendCaseSummaryToApplicantRadioButton(Decision decision)
        {
            switch(decision)
            {
                case Decision.Yes:
                    GetElementByXpath("//label[text()='Yes']").Click();
                    Console.WriteLine("-----------------clicked Yes radio button for sending the summary to applicant");
                    break;
                case Decision.No:
                    GetElementByXpath("//label[text()='No']").Click();
                    Console.WriteLine("-----------------clicked NO radio button for sending the summary to applicant");
                    break;
            } 
            return this;
        }

        public SubmissionMileStone SelectMethodOfContact(string contactMethod)
        {
            switch (contactMethod.ToLower())
            {
                case "email":
                    this.contactMethod = ContactMethod.Email;
                    SelectMethodOfContact(ContactMethod.Email);
                    break;
                case "print and post":
                    this.contactMethod = ContactMethod.PrintAndPost;
                    SelectMethodOfContact(ContactMethod.PrintAndPost);
                    break;
                case "both email and print & post":
                    this.contactMethod = ContactMethod.BothEmailAndPrintAndPost;
                    SelectMethodOfContact(ContactMethod.BothEmailAndPrintAndPost);
                    break;
            }
            return this;
        }

        public SubmissionMileStone SelectMethodOfContact(ContactMethod contactMethod)
        {
            Console.WriteLine("-----------------Trying to select the method of contact");
            if (BasePage.ApplicantPostalAddressExists)
            {
                var ele = GetElementByXpath(methodOfContactDropdown);
                while (ele.Text == "---Select a value---")
                {
                    ele.Click();
                    Console.WriteLine("-----------------clicked on the drop down so that the options are visible");
                    switch (contactMethod)
                    {
                        case ContactMethod.Email:
                            GetElementByXpath("//div/ul//li/div[text()='Email']").Click();
                            Console.WriteLine("-----------------clicked on Email option in the dropdown");
                            break;
                        case ContactMethod.PrintAndPost:
                            GetElementByXpath("//div/ul//li/div[text()='Print & Post']").Click();
                            Console.WriteLine("-----------------clicked on Print & Post option in the dropdown");
                            break;
                        case ContactMethod.BothEmailAndPrintAndPost:
                            GetElementByXpath("//div/ul//li/div[text()='Both Email and Print & Post']").Click();
                            Console.WriteLine("-----------------clicked on both Email and Print & Post option in the dropdown");
                            break;
                    }
                }
            }

            return this;
        }

        public SubmissionMileStone CopyEmailToCustomer(string decision)
        {
            if (BasePage.AppHasCustomer == true)
            {
                if (decision.ToLower() == "yes")
                {
                    GetElementByXpath(copyEmailToCustomer).Click();
                    Console.WriteLine("-----------------clciked on YES radio button to send an email to cutomer");
                }
            }
            return this;
        }

        public SubmissionMileStone CopyEmailToOtherRecipients(string decision, string email)
        {
            if (decision.ToLower() == "yes")
            {
                GetElementByXpath(copyEmailToOtherRecipients).Click();
                Console.WriteLine("-----------------clicked on copy email to other recipients");
                GetElementByXpath(copyEmailToOtherRecipientsTextBox).SendKeys(email);
                Console.WriteLine("-----------------send the email " + email + " to the sompy email to other recipients text field");
            }
            return this;
        }

        public SubmissionMileStone AnyOtherDocumentsToSend(string decision, string filename)
        {
            Console.WriteLine("-----------------Upload any other document: " + decision);
            switch (decision.ToLower())
            {
                case "yes":
                    if (filename != "" || filename != null)
                    {
                        AnyOtherDocumentsToSend(Decision.Yes);
                        GetElementByXpath(staticPageElement).Click();
                        if(HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                            UploadDocument(_driver.FindElement(By.XPath(uploadField)), filename);
                        Console.WriteLine("-----------------uploaded other document on Case Submission page");
                    }
                    else
                        throw new Exception($"File name to be uploaded cannot be {filename} on CaseSubmission Page");
                    break;
                case "no":
                    AnyOtherDocumentsToSend(Decision.No);
                    break;
            }
            return this;
        }

        public SubmissionMileStone AnyOtherDocumentsToSend(Decision decision)
        {
            switch (decision)
            {
                case Decision.Yes:
                    GetElementByXpath($"//span[contains(text(),'Are there other documents to send to')]/../../../../div[2]//label[text()='Yes']").Click();
                    break;
                case Decision.No:
                    GetElementByXpath($"//span[contains(text(),'Are there other documents to send to')]/../../../../div[2]//label[text()='No']").Click();
                    break;
            }
            return this;
        }
        
        public SubmissionMileStone EnterTheDatePrintAndPostDocumentWereSent(string date)
        {
            if (contactMethod == ContactMethod.BothEmailAndPrintAndPost || contactMethod == ContactMethod.PrintAndPost)
            {
                if (date == "")
                    EnterTheDatePrintAndPostDocumentWereSent();
                GetElementByXpath(printAndPostDocumentSentDate).SendKeys(date);
                Console.WriteLine("-----------------Entered the date on which Print and Post was done: " + date);
            }
            return this;
        }

        public SubmissionMileStone EnterTheDatePrintAndPostDocumentWereSent()
        {
            string currentDate = DateTime.Now.ToString("dd/MM/yyyy");
            GetElementByXpath(printAndPostDocumentSentDate).SendKeys(currentDate);
            Console.WriteLine("-----------------Entered the date on which print and post was done: " + currentDate);
            return this;
        }

        public SubmissionMileStone ClickReadyToEmailConfirmationCheckbox()
        {
            GetElementByXpath(readyToEmailCheckbox).Click();
            Console.WriteLine("-----------------clicked on ready to email checkbox");
            return this;
        }

        public MyDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Case Submission page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Case Submission Page Passes");
            }
            Console.WriteLine("-----------------clicked DONE button; Completed Case Submission Page");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new MyDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
    }
}
