﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class FinaliseCasePage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[text()='Finalise Case']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[text()='Finalise Case']";
        protected string finalisationReasonDropDown = "//div[@class='ContentLayout---content_layout']//div[text()='--Select a value--']";
        protected string finalisationComments = "//label[text()='The case is being finalised because....']/../../div[2]//textarea";
        protected string techoneCheckBox1 = "//p[contains(text(),'Financials')]/../../../../../div//div[@class='CheckboxGroup---choice_wrapper']";
        protected string techoneCheckBox2 = "//p[contains(text(),'Property Manager Agreements')]/../../../../../div//div[@class='CheckboxGroup---choice_wrapper']";
        protected string noApplicantNotificationOption = "//label[text()='No']";
        protected string confirmationCheckbox = "//label[contains(text(),'Confirm all actions')]";
        protected string doneButton = "//button[text()='Done']";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public FinaliseCasePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON CASE FINALISATION PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public FinaliseCasePage SelectFinalisationReason(string finalisationReason)
        {
            if (IsPageLoadComplete()
                && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear)
                && finalisationReason != "")
                do
                {
                    try
                    {
                        _driver.FindElement(By.XPath(finalisationReasonDropDown)).Click();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Stale Element on Case Finalisation Page " + e.Message);
                    }
                }
                while (!_driver.PageSource.Contains("Finalise Case"));

            if (finalisationReason.ToLower().Contains("no response from applicant"))
                GetElementByXpath($"//*[text()='No response from applicant']").Click();
            else if (finalisationReason.ToLower().Contains("withdrawn by applicant or customer"))
                GetElementByXpath($"//*[text()='Withdrawn by applicant or customer']").Click();
            return this;
        } 

        public FinaliseCasePage EnterComments(string comments)
        {
            GetElementByXpath(finalisationComments).SendKeys(comments);
            Console.WriteLine("-----------------entered comments");
            return this;
        }

        public FinaliseCasePage SelectNotToNotifyApplicantOrCustomer()
        {
            ScrollToView(_driver.FindElement(By.XPath(doneButton)));
            GetElementByXpath(noApplicantNotificationOption).Click();
            return this;
        }

        public FinaliseCasePage TickMandatoryCheckboxes()
        {
            ScrollToView(_driver.FindElement(By.XPath(doneButton)));
            GetElementByXpath(techoneCheckBox1).Click();
            GetElementByXpath(techoneCheckBox2).Click();
            GetElementByXpath(confirmationCheckbox).Click();
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }

            Console.WriteLine("-----------------clicked Done button; Completed Case Finalisation");

            return new CaseDashboardPage(_driver);
        }



    }
}
