﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class IntegrateWithObjectivePage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[text()='Integrate with Objective']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[text()='Integrate with Objective']";
        protected string fileNumberTextBox = "//input[@type = 'text']";
        protected string noCaseFolderOption = "//label[text()='No']";
        protected string commentsTextBox = "//textarea";
        protected string integrateButton = "//button[text()='INTEGRATE']";
        protected string refreshButton = "//button[text()='REFRESH']";
        //protected string spinningTextImage = "//em";
        //protected string integrationSuccessText = "//strong[contains (text(),'Integration was successful.')]";        
        protected string cancelButton = "//button[text()='Cancel']";
        protected string doneButton = "//button[text()='Done']";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public IntegrateWithObjectivePage(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON INTERGATION WITH OBJECTIVE PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsCaseIntegratedWithObjective(string fileNumber)
        {
            return GetElementByXpath($"//p[contains(text(),'{fileNumber}')]").Displayed;
        }

        public bool IsIntegrationComplete()
        {
            if (GetElementByXpath(refreshButton).Displayed)
                return false;
            else return true;
        }

        public IntegrateWithObjectivePage EnterFileNumber(string fileNumber)
        {
            GetElementByXpath(fileNumberTextBox).SendKeys(fileNumber);
            Console.WriteLine("-----------------entered the filenumber: "+ fileNumber);
            return this;
        }

        public IntegrateWithObjectivePage SelectNoCaseFolderOption()
        {
            GetElementByXpath(noCaseFolderOption).Click();
            Console.WriteLine("-----------------clicked on the case folder option as NO");
            return this;
        }

        public IntegrateWithObjectivePage EnterComments(string comments)
        {
            GetElementByXpath(commentsTextBox).SendKeys(comments);
            Console.WriteLine("-----------------entered the comments in comments textbox: "+ comments);
            return this;
        }

        public IntegrateWithObjectivePage ClickIntegrateButton()
        {
            GetElementByXpath(integrateButton).Click();
            Console.WriteLine("-----------------clicked Integrate button");
            GetElementByXpath(refreshButton).Click();
            Console.WriteLine("-----------------clicked Refresh button");
            int i = 0;
            while (_driver.PageSource.Contains("REFRESH") && i < 10)
            {
                if (_driver.PageSource.Contains("INTEGRATE"))
                    break;
                else
                    try
                    {
                        GetElementByXpath(refreshButton).Click();
                        Console.WriteLine("-----------------clicked Refresh button again");
                    }
                    catch (StaleElementReferenceException)
                    {
                        Console.WriteLine("Stale exception thrown");
                    }
                Thread.Sleep(10000);
                i++;
            }
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            ScrollToView(_driver.FindElement(By.XPath(doneButton)));
            GetElementByXpath(doneButton).Click();
            Console.WriteLine("-----------------clicked DONE button");
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Integrate With Objective page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Integrate With Objective throws no validation error");
            }

            Console.WriteLine("Completed Integrate With Objective");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new CaseDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
    }
}
