﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class TeamDashboardPage : BasePage
    {
        IWebDriver _driver = null;

        protected override By IsPageLoadedBy => By.XPath("//title[contains(text(),'Team Dashboard - iWMS DoL Officer Site')]");
        //protected IWebElement ManagerName => GetElementByXpath("//span[contains(text(),'Sophia')]");
        protected string filtersExpand = "//a[contains(text(),'Filters')]";
        protected string caseNumberFilterTextBox = "//*[text()='Case Number']/../../div[2]/div/input";
        protected string applyFilterButton = "//button[contains(text(),'Apply')]";
        protected string allocateNewCaseTaskGridLink = "//a[text()='Allocate New Case']";
        protected string caseNumberTaskGridLink = "//a[.='{caseNumber}']";
        protected string reallocateTaskLabel = "//h2[(text()='Reallocate Task')]";

        public TeamDashboardPage(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                _driver = driver;
                Console.WriteLine("ON TEAM DASHBOARD PAGE");
            }
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsManagerNameDisplayed(string name)
        {
            var NameElement = GetElementByXpath($"//strong[contains(text(),'{name}')]");
            return NameElement.Displayed;
        }

        public bool IsNewCaseFoundInTeamDashboard(string caseNumber)
        {
            var caseNumberElement = GetElementByXpath($"//a[.='{caseNumber}']");
            return caseNumberElement.Displayed;
        }

        public TeamDashboardPage FilterAndSearchByCaseNumber(string caseNumber)
        {
            bool displayed = false;
            int i = 0;
            if(HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                GetElementByXpath(filtersExpand).Click();
            Console.WriteLine("-----------------Clicked on Filters to expand filters");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                GetElementByXpath(caseNumberFilterTextBox).Clear();
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                GetElementByXpath(caseNumberFilterTextBox).SendKeys(caseNumber);
                Console.WriteLine("-----------------Entered the Case Number for search " + caseNumber);
            }
            GetElementByXpath(applyFilterButton).Click();
            Console.WriteLine("-----------------Clicked the Apply button to search for the case number");

            while ((!HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear)
                || !displayed) && i < 10)
            {
                try
                {
                    i++;
                    Console.WriteLine("-----------------waiting for the case number to appear in search results");
                    displayed = _driver.FindElement(By.XPath($"//a[.='{caseNumber}']")).Displayed;
                    Console.WriteLine("-----------------case number displayed: " + displayed);
                    GetElementByXpath(applyFilterButton).Click();
                    Console.WriteLine("-----------------click Apply button again");
                }
                catch (Exception e)
                {
                    Console.WriteLine("-----------------Searching for the case number threw an exception");
                    Console.WriteLine(e.Message);
                }
            }
            return this;
        }


        public AllocateNewCasePage ClickAllocateNewCaseTaskGridLink()
        {
            GetElementByXpath(allocateNewCaseTaskGridLink).Click();
            Console.WriteLine("-----------------clicked on Allocate New case Task on Team dashboard");
            return new AllocateNewCasePage(_driver);
        }

        public CaseDashboardPage ClickCaseNumberTaskGridLink(string caseNumber)
        {
            GetElementByXpath($"//a[.='{caseNumber}']").Click();
            Console.WriteLine("-----------------clicked on Case Number on Team Dashboard");
            return new CaseDashboardPage(_driver);
        }


    }
}
