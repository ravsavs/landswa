﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class AdditionalInformationMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Additional Information']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Additional Information')]";

        protected string additionalInfoTextarea = "//textarea";
        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string continueButton = "//button[text()='Continue']";
        protected string caseReferenceNumberField = "//label[text()='Case Reference']/../../div[2]//input";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        protected string hasTheApplicantProvidedOwnCaseNumber = null;

        public AdditionalInformationMileStone(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON ADDITIONAL INFO PAGE");
        }

        public AdditionalInformationMileStone EnterAdditionalInfo(string info)
        {
            GetElementByXpath(additionalInfoTextarea).SendKeys(info);
            Console.WriteLine("-----------------entered additional info in text area: " + info);
            return this;
        }

        public AdditionalInformationMileStone UploadDocument(string fileName)
        {
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("-----------------clicked on static element");
            UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
            Console.WriteLine("-----------------uploaded the document");
            return this;
        }

        public AdditionalInformationMileStone HasTheApplicantProvidedOwnCaseNo(string decision)
        {
            hasTheApplicantProvidedOwnCaseNumber = decision;
            _driver.FindElement(By.XPath($"//label[text()='{decision}']")).Click();
            Console.WriteLine("-----------------clicked on the deciion for having Own Case Number: " + decision);
            return this;
        }

        public AdditionalInformationMileStone EnterCaseReferenceNo(string caseNo)
        {
            if (hasTheApplicantProvidedOwnCaseNumber.ToLower() == "yes")
            {
                GetElementByXpath(caseReferenceNumberField).SendKeys(caseNo);
                Console.WriteLine("-----------------entered the case number");
            }
            return this;
        }

        public ReviewMileStone ClickContinueButton()
        {
            GetElementByXpath(continueButton).Click();
            Console.WriteLine("-----------------clicked continue button AddtionalInformation Page");

            GetElementByXpath(continueButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Case Submission page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Case Submission Page Passes");
            }
            Console.WriteLine("-----------------clicked DONE button; Completed Case Submission Page");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new ReviewMileStone(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }
    }
}
