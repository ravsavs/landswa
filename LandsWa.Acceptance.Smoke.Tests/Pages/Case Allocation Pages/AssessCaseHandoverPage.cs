﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    class AssessCaseHandoverPage : BasePage
    {
        private IWebDriver _driver;
        protected override By IsPageLoadedBy => By.XPath("");

        public AssessCaseHandoverPage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }
    }
  
}
