﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;
using System.Threading;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class AllocateNewCasePage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Allocate New Case')]");
        private IWebDriver _driver;
        protected string staticPageElement = "//h1[contains(text(),'Allocate New Case')]";
        protected string acceptButton = "//button[contains(text(),'Accept')]";
        protected string caseActionDropDown = "//div[@class='ContentLayout---content_layout']//div[text()='...Select a value ---']";

        //Allocate Case to other team 
        protected string otherTeamDropDown = "//*[text()='Allocate Case to another Team']/../../div[2]";
        //Assign Case to officer in Team fields
        protected string officerField = "//span[text()='Officer']/../..//input";

        protected string searchedOfficer = "//span[text()='1 result available. Use enter to select.']";
        protected string identifyWIDropDown = "//span[text()='Identify Work Instruction']/../../div[2]/div/div";

        protected string dueDateField = "//label[text()='Due Date']/../../div[2]//input";
        protected string commentsTextBox = "//textarea";

        protected string doneButton = "//button[contains(text(),'Done')]";
        protected string cancelButton = "//button[contains(text(),'Cancel')]";
        protected string saveExitButton = "//button[contains(text(),'Save & Exit')]";

        protected string officerName = null;
        protected static string workInstruction = null;
        protected string currentDate = DateTime.Now.ToString("dd/MM/yyyy");
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public IWebElement ele = null;

        public AllocateNewCasePage(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                _driver = driver;
                GetElementByXpath(staticPageElement).Click();
                Console.WriteLine("ON ALLOCATED NEW CASE PAGE");
            }
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsAllocateNewCaseTaskForNewCase(string caseNumber)
        {
            return GetElementByXpath($"//a[.='{caseNumber}']").Displayed;
        }

        public AllocateNewCasePage ClickAcceptTask()
        {
            GetElementByXpath(acceptButton).Click();
            Console.WriteLine("-----------------clicked on Accept button for New Case Allocation task");
            return this;
        }

        public AllocateNewCasePage SelectCaseAllocationAction(string action)
        {
            switch (action)
            {
                case "WithinTeam":
                    {
                        SelectCaseAllocationAction(CaseAllocationAction.WithinTeam);
                        Console.WriteLine("-----------------selected case allocation within team option");
                        break;
                    }
                case "OutsideTeam":
                    {
                        SelectCaseAllocationAction(CaseAllocationAction.OutsideTeam);
                        Console.WriteLine("-----------------selected case allocation to outside team");
                        break;
                    }
            }
            return this;
        }

        public AllocateNewCasePage SelectCaseAllocationAction(CaseAllocationAction action)
        {
            if (IsPageLoadComplete() && HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                do
                {
                    try
                    {
                        _driver.FindElement(By.XPath(caseActionDropDown)).Click();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Stale Element on Case Allocation Page " + e.Message);
                    }
                }
                while (!_driver.PageSource.Contains("Assign Case to an Officer in my team"));

            switch (action)
            {
                case CaseAllocationAction.WithinTeam:
                    {

                        GetElementByXpath($"//*[text()='Assign Case to an Officer in my team']").Click();
                        break;
                    }
                case CaseAllocationAction.OutsideTeam:
                    {
                        GetElementByXpath($"//*[text()='Allocate Case to another team']").Click();
                        break;
                    }
            }
            return this;
        }


        public AllocateNewCasePage EnterOfficerName(string officerName)
        {
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(officerField)));
            var element = GetElementByXpath(officerField);
            element.SendKeys(officerName);
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath(searchedOfficer)));
            element.SendKeys(Keys.Enter);
            Console.WriteLine("-----------------selected officer from the list: "+ officerName);
            return this;
        }

        public AllocateNewCasePage EnterWI(string workInstruction)
        {
            var element = GetElementByXpath(identifyWIDropDown);
            element.Click();
            element.SendKeys(workInstruction);
            Wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath($"//span[contains(text(),'{workInstruction}')]")));
            element.SendKeys(Keys.Enter);
            Console.WriteLine("-----------------selected WI");
            return this;
        }

        public AllocateNewCasePage EnterComments(string comments)
        {
            GetElementByXpath(commentsTextBox).SendKeys(comments);
            Console.WriteLine("-----------------entered comments");
            return this;
        }

        public AllocateNewCasePage EnterDueDate(string dueDate)
        {
            if (dueDate == null)
            {
                dueDate = DateTime.Now.AddDays(2).ToString("dd/MM/yyyy");
                GetElementByXpath(dueDateField).SendKeys(dueDate);
            }
            else GetElementByXpath(dueDateField).SendKeys(dueDate);
            Console.WriteLine("-----------------entered due date");
            return this;
        }

        public AllocateNewCasePage EnterOtherTeamName(string otherTeamName)
        {
            GetElementByXpath(otherTeamDropDown).Click();
            GetElementByXpath($"//*[text()='{otherTeamName}']").Click();
            Console.WriteLine("-----------------entered other team name");
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }
            Console.WriteLine("-----------------clicked DONE button; Completed Allocate New Case");
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                return new CaseDashboardPage(_driver);
            else
                throw new Exception($"Progress Bar did not disappear within {SecondsWaitForProgressBarToDisappear} seconds");
        }


    }
}
