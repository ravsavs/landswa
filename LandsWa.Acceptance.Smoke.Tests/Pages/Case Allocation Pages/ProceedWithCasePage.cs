﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ProceedWithCasePage : BasePage
    {
        private IWebDriver _driver;
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Proceed With Case')]");
        protected string staticPageElement = "//h1[contains(text(),'Proceed With Case')]";
        protected string confirmationCheckbox = "//label[@class = 'CheckboxGroup---choice_label']";
        protected string cancelButton = "//button[text()='CANCEL']";
        protected string doneButton = "//button[text()='DONE']";
        protected string errorMessage = "//*[contains(text(), 'A value is required')]";

        public ProceedWithCasePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            GetElementByXpath(staticPageElement).Click();
            Console.WriteLine("ON PROCEED WITH CASE PAGE");
        }

        public bool IsPageLoadComplete()
        {
            return IsPageLoaded();
        }

        public bool IsProceedWithCaseTaskForNewCase(string caseNumber)
        {
            return GetElementByXpath($"//a[.='{caseNumber}']").Displayed;
        }

        public ProceedWithCasePage ClickConfirmationCheckbox()
        {
            GetElementByXpath(confirmationCheckbox).Click();
            Console.WriteLine("-----------------clicked n Confirmation checkbox");
            return this;
        }

        public CaseDashboardPage ClickDoneButton()
        {
            GetElementByXpath(doneButton).Click();
            try
            {
                if (_driver.FindElement(By.XPath(errorMessage)).Displayed)
                    throw new Exception("Error displayed on Allocate New Case page");
            }
            catch (Exception e)
            {
                Console.WriteLine("Page Validation on Allocate New Case Page Passes");
            }
            Console.WriteLine("-----------------clicked on DONE button; Completed Allocate New Case");

            return new CaseDashboardPage(_driver);
        }
    }
}
