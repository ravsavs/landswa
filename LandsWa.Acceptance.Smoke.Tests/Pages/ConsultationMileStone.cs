﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    public class ConsultationMileStone : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//a[text()='Consultation']/span[text()='Current Step']");
        private IWebDriver _driver;
        protected string staticPageElement = "//h2[contains(text(), 'Consultation Details')]";
        protected string checkbox = "//tbody/tr/td[1]/div";
        protected string PIHConsultedRadioButtonYes = "//strong[text()='Has Primary Interest Holder/Proprietor been consulted? *']/../../../../../../../..//div[@role='radiogroup']/div[1]";
        protected string PIHConsultedRadioButtonNo = "//strong[text()='Has Primary Interest Holder/Proprietor been consulted? *']/../../../../../../../..//div[@role='radiogroup']/div[2]";

        protected string LGABeenConsultedRadioButton = "//strong[text()='Has LGA been consulted? *']/../../../../../../../..//div[@role='radiogroup']/div[1]";
        protected string LGANotBeenConsultedRadioButton = "//strong[text()='Has LGA been consulted? *']/../../../../../../../..//div[@role='radiogroup']/div[2]";
        protected string LGAConsultedNotApplicableRadioButton = "//strong[text()='Has LGA been consulted? *']/../../../../../../../..//div[@role='radiogroup']/div[3]";

        protected string uploadField = "//input[@class='MultipleFileUploadWidget---ui-inaccessible']";
        protected string updateButton = "//button[text()='UPDATE']";
        protected string commentArea = "//textarea";
        protected string otherAgenciesBeenConsultedForInternalCase = "//label[text()='Yes']";
        protected string otherAgenciesNotBeenConsultedForInternalCase = "//label[text()='No']";
        protected string addOtherAgencyImg = "//img[@aria-label='Add Other Agencies']";
        protected string otherAgencyTextBox = "//input[@aria-autocomplete='list']";
        protected string selectedAgency = "//span[@class='PickerTokenWidget---label']";
        protected string outcomeTextBox = "//span[text()='Outcome']/../../../../div[2]//input";

        protected string continueButton = "//button[text()='Continue']";

        protected string agencyName = null;

        private static int updateButtonClickCounter;

        public ConsultationMileStone(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                _driver = driver;
                GetElementByXpath(staticPageElement).Click();
                updateButtonClickCounter = 0;
                Console.WriteLine("ON CONSULTATION PAGE/JUST CLICKED STATIC ELEMENT - HEADING: Consultation Details");
            }
        }

        public ConsultationMileStone ClickPIHCheckboxToConsult(string pihName)
        {
            if (pihName != "")
            {
                _driver.FindElement(By.XPath($"//p[text()='{pihName}']/../../td[1]/div")).Click();
                Console.WriteLine("------------------------Clicked PIH on Consultation Page");
            }
            return this;
        }

        public ConsultationMileStone ClickHasPIHbeenConsultedRadioButton(string desc)
        {
            switch (desc.ToLower())
            {
                case "yes":
                    GetElementByXpath(PIHConsultedRadioButtonYes).Click();
                    break;
                case "no":
                    GetElementByXpath(PIHConsultedRadioButtonNo).Click();
                    break;
                default:
                    break;
            }
            Console.WriteLine("------------------------Clicked Radio Button for the Q - If PIH has been conculted");
            return this;
        }

        public ConsultationMileStone ClickLGACheckboxToConsult()
        {
            GetElementByXpath(checkbox).Click();
            Console.WriteLine("------------------------Clicked LGA Checkbox on Consultation Page");
            return this;
        }

        public ConsultationMileStone ClickLGACheckboxToConsult(string lgaName)
        {
            _driver.FindElement(By.XPath($"//p[text()='{lgaName}']/../../td[1]/div")).Click();
            Console.WriteLine("------------------------Clicked LGA Checkbox on Consultation Page");
            return this;
        }

        public ConsultationMileStone HasLGABeenConsultedRadioButtonResponse(string desc)
        {
            if(HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            switch (desc.ToLower())
            {
                case "yes":
                    HasLGABeenConsultedRadioButtonResponse(Decision.Yes);
                    break;
                case "no":
                    HasLGABeenConsultedRadioButtonResponse(Decision.No);
                    break;
                case "not applicable":
                    HasLGABeenConsultedRadioButtonResponse(Decision.NotApplicable);
                    break;
            }
            Console.WriteLine("------------------------Clicked LGA has been consulted or not readio button on Consultation Page");
            return this;
        }

        public ConsultationMileStone HasLGABeenConsultedRadioButtonResponse(Decision desc)
        {
            switch(desc)
            {
                case Decision.Yes:
                    GetElementByXpath(LGABeenConsultedRadioButton).Click();
                    break;
                case Decision.No:
                    GetElementByXpath(LGANotBeenConsultedRadioButton).Click();
                    break;
                case Decision.NotApplicable:
                    GetElementByXpath(LGAConsultedNotApplicableRadioButton).Click();
                    break;
            }
            return this;
        }

        public ConsultationMileStone UploadDocument(string fileName)
        {
            if (fileName != "")
            {
                GetElementByXpath(staticPageElement).Click();
                if (GetElementByXpath("//button[text()='Upload']").Displayed)
                    UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
                else
                {
                    Thread.Sleep(1000);
                    if (GetElementByXpath("//button[text()='Upload']").Displayed)
                        UploadDocument(_driver.FindElement(By.XPath(uploadField)), fileName);
                    else
                        throw new Exception("Upload file input field does not exist even after 1.5 second wait");
                }
            }
            Console.WriteLine("------------------------Uploaded document on Consultation Page");
            return this;
        }

        public ConsultationMileStone EnterCommentsReceivedFromLga(string comments)
        {
            GetElementByXpath(commentArea).SendKeys(comments);
            return this;
        }


        public ConsultationMileStone ClickUpdateButton()
        {
            Console.WriteLine("------------------------Trying to click 'UPDATE' button on Consultation Page");
            IWebElement ele = null;
            int i = 0;
            string xpathForYesText = "(//tbody/tr/td[3]/p)[2]";
            GetElementByXpath(updateButton).Click();
            if (updateButtonClickCounter == 0)
            {
                xpathForYesText = "(//tbody/tr/td[3]/p)[1]";
                updateButtonClickCounter++;
            }

            if (HasProgressBarDisappearedWithinSeconds(120))
            {

                ele = GetElementByXpath(xpathForYesText);
                try
                {
                    while (!(ele.Text == "Yes" || ele.Text == "No") && i < 10)
                    {
                        try
                        {
                            i++;
                            GetElementByXpath(updateButton).Click();
                            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
                                ele = GetElementByXpath(xpathForYesText);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                }
                catch(StaleElementReferenceException exception)
                {
                    Console.WriteLine("STALE ELEMENT EXCEPTION ON CONSULTATION PAGE, WHILE TRYING TO CLICK UPDATE BUTTON " + exception.Message);
                }
            }

            Console.WriteLine("------------------------Finished clicking 'UPDATE' button on Consultation Page");
            return this;
        }


        public ConsultationMileStone AreOtherAgenciesConsultedWithThisInternalCase(string decision)
        {
            if(BasePage.caseType == CaseType.Internal)
            switch (decision.ToLower())
            {
                case ("yes"):
                    AreOtherAgenciesConsultedWithThisInternalCase(Decision.Yes);
                    break;
                case ("no"):
                    AreOtherAgenciesConsultedWithThisInternalCase(Decision.No);
                    break;
                default:
                    return null;
            }
            return this;
        }

        public ConsultationMileStone AreOtherAgenciesConsultedWithThisInternalCase(Decision decision)
        {
            switch (decision)
            {
                case (Decision.Yes):
                    GetElementByXpath(otherAgenciesBeenConsultedForInternalCase).Click();
                    break;
                case (Decision.No):
                    GetElementByXpath(otherAgenciesNotBeenConsultedForInternalCase).Click();
                    break;
                default:
                    return null;
            }
            return this;
        }

        public ConsultationMileStone ClickOnAddOtherAgencyImage()
        {
            GetElementByXpath(addOtherAgencyImg).Click();
            return this;
        }

        public ConsultationMileStone AddOtherAgency(string agencyName)
        {
            var ele = GetElementByXpath(otherAgencyTextBox);
            ele.SendKeys(agencyName);
            Thread.Sleep(1000);
            ele.SendKeys(Keys.ArrowDown);
            Thread.Sleep(1000);
            ele.SendKeys(Keys.Enter);
            this.agencyName = GetElementByXpath(selectedAgency).Text;
            return this;
        }

        public ConsultationMileStone EnterOutcome(string outcome)
        {
            GetElementByXpath(outcomeTextBox).SendKeys(outcome);
            return this;
        }

        public ConsultationMileStone UploadCorrespondanceDocument(string fileName)
        {
            UploadDocument(fileName);
            return this;
        }

        public AdditionalInformationMileStone ClickContinueButton()
        {
            GetElementByXpath(continueButton).Click();
            Console.WriteLine("------------------------Clicked Continue button on Consultation Page");
            return new AdditionalInformationMileStone(_driver);
        }
    }
}
