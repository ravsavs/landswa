﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Pages
{
    internal class AssignApplicantCustomerPage : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'Assign Applicant/Customer to the New Case')]");
        private IWebDriver _driver;
        protected string IsCustomerTheApplicantNo = "//input[@value='No']/..";
        protected string staticPageHeading = "//h2[contains(text(),'Search for')]";
        protected string enabledContinueButton = "//button[contains(text(),'Continue')]";
        ApplicantSearch applicantSearch;
        CustomerSearch customerSearch;

        public AssignApplicantCustomerPage(IWebDriver driver) : base(driver)
        {
            if (HasProgressBarDisappearedWithinSeconds(SecondsWaitForProgressBarToDisappear))
            {
                _driver = driver;
                applicantSearch = new ApplicantSearch(_driver);
                customerSearch = new CustomerSearch(_driver);
                Console.WriteLine("ON SEARCH APPLICANT CUSTOMER PAGE");
            }
        }

        public AssignApplicantCustomerPage SearchAnApplicantWithName(string name)
        {
            if (name == "") throw(new Exception("Empty Applicant Name cannot be searched"));
            applicantSearch.EnterFirstName(name)
                .ClickApplyButton();
            return this;
        }

        public AssignApplicantCustomerPage SearchAnApplicantWithName(string firstName, string lastName)
        {
            if (firstName == "" && lastName == "") throw (new Exception("Empty Applicant Name cannot be searched"));
            applicantSearch.EnterFirstName(firstName)
                .EnterLastName(lastName)
                .ClickApplyButton();
            return this;
        }

        public AssignApplicantCustomerPage SearchACustomerWithName(string firstName)
        {
            if (firstName == "") throw (new Exception("Empty Applicant Name cannot be searched"));
            customerSearch.EnterFirstName(firstName)
                .ClickApplyButton();
            return this;
        }

        public AssignApplicantCustomerPage SearchACustomerWithName(string firstName, string lastName)
        {
            if (firstName == "" && lastName == "") throw (new Exception("Empty Applicant Name cannot be searched"));
            customerSearch.EnterFirstName(firstName)
                .EnterLastName(lastName)
                .ClickApplyButton();
            return this;
        }

        public AssignApplicantCustomerPage SelectTheApplicantFromSearchResultWithName(string name)
        {
            applicantSearch.SelectApplicantFromResults(name);
            return this;
        }

        public AssignApplicantCustomerPage SelectTheCustomerFromSearchResultWithName(string name)
        {
            customerSearch.SelectCustomerFromResults(name);
            return this;
        }

        public AssignApplicantCustomerPage CheckApplicantIsNotCustomer()
        {
            GetElementByXpath(IsCustomerTheApplicantNo).Click();
            return this;
        }

        public ApplicantDetailsMileStone Continue()
        {
            GetElementByXpath(staticPageHeading).Click();
            GetElementByXpath(enabledContinueButton).Click();
            return new ApplicantDetailsMileStone(_driver);
        }

    }

    internal class ApplicantSearch : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h2[contains(text(),'Search for Applicant')]");
        private IWebDriver _driver;
        protected string firstNameField = "(//*[text()='First Name']/../..//input)[1]";
        protected string lastNameField = "(//*[text()='Last Name']/../..//input)[1]";
        protected string disabledApplyButton = "//div[@class='Button---disabled_btn_glass']/following-sibling::button[text()='Apply']";
        protected string enabledApplyButton = "//button[contains(text(),'Apply')]";
        protected string staticPageHeading = "//h2[contains(text(),'Search for')]";
        protected string searchResultText = "//h2[text()='Search Results']";
        protected string searchResultFirstColumnHeading = "//div[contains(text(),'First Name')]";

        protected string disabledContinueButton = "//div[@class='Button---disabled_btn_glass']/following-sibling::button[text()='Continue']";

        public ApplicantSearch(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public ApplicantSearch EnterFirstName(string firstName)
        {
            GetElementByXpath(firstNameField).SendKeys(firstName);
            Console.WriteLine("-----------------entered the first name for the applicant");
            return this;
        }

        public ApplicantSearch EnterLastName(string lastName)
        {
            GetElementByXpath(lastNameField).SendKeys(lastName);
            Console.WriteLine("-----------------entered the last name for the applicant");
            return this;
        }

        public ApplicantSearch ClickApplyButton()
        {
            GetElementByXpath(staticPageHeading).Click();
            GetElementByXpath(enabledApplyButton).Click();
            Console.WriteLine("-----------------clicked the apply button");
            return this;
        }

        public bool IsApplicantSearchResultEmpty()
        {
            IWebElement ele;
            try
            {
                ele = GetElementByXpath(searchResultFirstColumnHeading);
            }
            catch(Exception e)
            {
                Console.WriteLine("Search Result is empty " + e.Message);
                return false;
            }
            return true;
        }

        public bool ApplicantPresentInSearchResult(string name)
        {
            IWebElement ele;
            try
            {
                ele = GetElementByXpath($"//p[contains(text(),'{name}')]");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Search Result does not contain the name - {name} " + e.Message);
                return false;
            }
            return true;
        }

        public void SelectApplicantFromResults(string name)
        {
            
            IWebElement ele = null;
            try
            {
                ele = GetElementByXpath($"(//div[@class='SectionLayout2---sectionLayout']//p[contains(text(),'{name}')]/../../td[1]/div)[1]");
                IWebElement postalAddressField = GetElementByXpath($"(//div[@class='SectionLayout2---sectionLayout']//p[contains(text(),'{name}')]/../../td[5])[1]/p");
                if (postalAddressField.Text != "")
                    BasePage.ApplicantPostalAddressExists = true;
            }
            catch (Exception)
            {
                Console.WriteLine($"Search Result does not contain the name - {name}");
            }
            Console.WriteLine("-----------------clicked the first result from the search applicant result list");
            ele.Click();
        }
    }

    internal class CustomerSearch : BasePage
    {
        protected override By IsPageLoadedBy => By.XPath("//h1[contains(text(),'//h2[contains(text(),'Search for Customer')]");
        private IWebDriver _driver;

        // The Customer
        protected string IsCustomerTheApplicantYes = "//input[@value='Yes']";
        protected string customerFirstNameField = "(//*[text()='First Name']/../..//input)[3]";
        protected string customerLastNameField = "(//*[text()='Last Name']/../..//input)[3]";
        protected string CustomerEnabledApplyButton = "(//button[contains(text(),'Apply')])[2]";
        protected string staticPageHeading = "//h2[contains(text(),'Search for Customer')]";

        public CustomerSearch(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public CustomerSearch EnterFirstName(string customerFirstName)
        {
            if(caseType == CaseType.Internal)
                GetElementByXpath("//*[text()='First Name']/../..//input").SendKeys(customerFirstName);
            else
                GetElementByXpath(customerFirstNameField).SendKeys(customerFirstName);
            Console.WriteLine("-----------------entered the first name for the customer");
            return this;
        }

        public CustomerSearch EnterLastName(string customerLastName)
        {
            if (caseType == CaseType.Internal)
                GetElementByXpath("//*[text()='Last Name']/../..//input").SendKeys(customerLastName);
            else
                GetElementByXpath(customerLastNameField).SendKeys(customerLastName);
            Console.WriteLine("-----------------entered the last name for the customer");
            return this;
        }

        public void SelectCustomerFromResults(string name)
        {

            IWebElement ele = null;
            try
            {
                ele = GetElementByXpath($"(//div[@class='SectionLayout2---sectionLayout']//p[contains(text(),'{name}')]/../../td[1]/div)[1]");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Search Result does not contain the name - {name}");
                Console.WriteLine(e.Message);
            }
            ele.Click();
            Console.WriteLine("-----------------clicked on First Customer from the result list");
            BasePage.AppHasCustomer = true;
        }

        public CustomerSearch ClickApplyButton()
        {
            GetElementByXpath(staticPageHeading).Click();
            if (caseType == CaseType.Internal)
                GetElementByXpath("//button[contains(text(),'Apply')]").Click();
            else
                GetElementByXpath(CustomerEnabledApplyButton).Click();
            Console.WriteLine("-----------------clicked Apply button");
            return this;
        }
    }
}
