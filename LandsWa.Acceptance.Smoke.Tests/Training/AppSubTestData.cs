﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Resources
{
    public class AppSubTestData
    {
        public string SerialNumber                                  { get; set; }
        public string UserName                                      { get; set; }
        public string Password                                      { get; set; }
        public string UserFirstName                                 { get; set; }
        public string UserFullName                                  { get; set; }
        public string Role                                          { get; set; }
        public string CaseInitiation                                { get; set; }
        public string AssociatedCustomer                            { get; set; }
        public string ApplicantFirstName                            { get; set; }
        public string ApplicantLastName                             { get; set; }
        public string IsApplicantTheCustomer                        { get; set; }
        public string CustomerFirstName                             { get; set; }
        public string CustomerLastName                              { get; set; }
        public string ProofOfConsentFileName                        { get; set; }
        public string RequestType                                   { get; set; }
        public string RequestCategory                               { get; set; }
        public string RequestDescription                            { get; set; }
        public string CaseDescription                               { get; set; }
        public string CLEF                                          { get; set; }
        public string Signed                                        { get; set; }
        public string DateSigned                                    { get; set; }
        public string Position                                      { get; set; }
        public string ProjectName                                   { get; set; }
        public string CaseDueDate                                   { get; set; }
        public string DateReceived                                  { get; set; }
        public string ApplicantRequestDocumentName                  { get; set; }
        public string EventFromDate                                 { get; set; }
        public string EventFromTime                                 { get; set; }
        public string EventToDate                                   { get; set; }
        public string EventToTime                                   { get; set; }
        public string EventPurpose                                  { get; set; }
        public string EventLandSize                                 { get; set; }
        public string EvenLayoutDocument                            { get; set; }
        public string AlterationToSite                              { get; set; }
        public string SitePlanDocument                              { get; set; }
        public string NoOfVisitors                                  { get; set; }
        public string NoOfEmployees                                 { get; set; }
        public string NoOfArtists                                   { get; set; }
        public string Revenue                                       { get; set; }
        public string Grants                                        { get; set; }
        public string Expenses                                      { get; set; }
        public string GrantDocumentName                             { get; set; }
        public string IsLandRecordAssociated                        { get; set; }
        public string LandGeneralInformation                        { get; set; }
        public string LandMapDoc                                    { get; set; }
        public string LotNumber                                     { get; set; }
        public string PlanNumber                                    { get; set; }
        public string Pin                                           { get; set; }
        public string ReserveNumber                                 { get; set; }
        public string ReserveClass                                  { get; set; }
        public string ReservePurpose                                { get; set; }
        public string IsLandTitleAvailable                          { get; set; }
        public string Volume                                        { get; set; }
        public string Folio                                         { get; set; }
        public string PIHName                                       { get; set; }
        public string IsCustomerPIH                                 { get; set; }
        public string StreetNumber                                  { get; set; }
        public string StreetName                                    { get; set; }
        public string StreetType                                    { get; set; }
        public string Suburb                                        { get; set; }
        public string Lga                                           { get; set; }
        public string LandRecordComment                             { get; set; }
        public string GeneralInfoComment                            { get; set; }
        public string MultipleLandRecords                           { get; set; }
        public string PIHConsulted                                  { get; set; }
        public string LgaConsulted                                  { get; set; }
        public string LgaCorrespondenceDocumentName                 { get; set; }
        public string LgaComments                                   { get; set; }
        public string OtherAgenciesConsultation                     { get; set; }
        public string AgencyName                                    { get; set; }
        public string Outcome                                       { get; set; }
        public string ExternalAgencyConsultationDoc                 { get; set; }
        public string AdditionalInfo                                { get; set; }
        public string AdditionalDocumentName                        { get; set; }
        public string ApplicantsOwnCaseReference                    { get; set; }
        public string OwnCaseReferenceNumber                        { get; set; }
        public string SendSubmissionSummary                         { get; set; }
        public string MethodOfContact                               { get; set; }
        public string CopyEmailToCustomer                           { get; set; }
        public string CopyEmailToOtherRecipient                     { get; set; }
        public string OtherRecipeientEmail                          { get; set; }
        public string OtherDocumentsToSend                          { get; set; }
        public string OtherDocumentName                             { get; set; }
        public string PrintAndPostDate                              { get; set; }
        public string ConfirmReadyToEmail                           { get; set; }
        public string LandRecords                                   { get; set; }
        public string OtherAgenciesConsulted                        { get; set; }
        public string App_EmailAddress                              { get; set; }
        public string App_PhoneNumber                               { get; set; }
        public string App_MobileNumber                              { get; set; }
        public string App_Organisation                              { get; set; }
        public string App_ABN                                       { get; set; }
        public string App_ACN                                       { get; set; }
        public string App_ICN                                       { get; set; }
        public string App_Address                                   { get; set; }
        public string App_Country                                   { get; set; }
        public string App_State                                     { get; set; }
        public string App_Suburb                                    { get; set; }
        public string App_PostCode                                  { get; set; }
        public string Cust_EmailAddress                             { get; set; }
        public string Cust_PhoneNumber                              { get; set; }
        public string Cust_MobileNumber                             { get; set; }
        public string Cust_Organisation                             { get; set; }
        public string Cust_ABN                                      { get; set; }
        public string Cust_ACN                                      { get; set; }
        public string Cust_ICN                                      { get; set; }
        public string Cust_Address                                  { get; set; }
        public string Cust_Country                                  { get; set; }
        public string Cust_State                                    { get; set; }
        public string Cust_Suburb                                   { get; set; }
        public string Cust_PostCode                                 { get; set; }
        public string EndPoint                                      { get; set; }
        public string CaseAllocation                                { get; set; }
        public string CaseAllocationManager                         { get; set; }
        public string CaseAllocationManagerPassword                 { get; set; }
        public string CaseAllocationAction                          { get; set; }
        public string CaseAllocationWI                              { get; set; }
        public string CaseAllocationComments                        { get; set; }
        public string CaseAllocationDueDate                         { get; set; }
        public string ProceedWithCase                               { get; set; }
        public string PriorityRating                                { get; set; }
        public string PriorityRatingWI                              { get; set; }
        public string PriorityRatingOption                          { get; set; }
        public string PriorityRatingManagerDecision                 { get; set; }
        public string PriorityRatingManagerOption                   { get; set; }
        public string PriorityRatingManagerComments                 { get; set; }
        public string Integration                                   { get; set; }
        public string IntegrationFileNumber                         { get; set; }
        public string IntegrateWithObjectiveComments                { get; set; }
        public string CreateTask                                    { get; set; }
        public string NoOfCreateTasks                               { get; set; }
        public string CreateTaskAllocatedOfficer1                   { get; set; }
        public string CreateTasksWorkInstruction1                   { get; set; }
        public string CreateTasksTaskName1                          { get; set; }
        public string CreateTasksComments1                          { get; set; }
        public string CreateTaskDueDate1                            { get; set; }
        public string CreateTaskDoWeNeedUploadFile1                 { get; set; }
        public string CreateTaskFileUpload1                         { get; set; }
        public string CreateTaskTenureSearchItem11                  { get; set; }
        public string CreateTaskTenureSearchItem12                  { get; set; }
        public string CreateTaskTenureSearchItem13                  { get; set; }
        public string CreateTaskTenureSearchItem14                  { get; set; }
        public string CreateTaskTenureSearchItem15                  { get; set; }
        public string CreateTaskAllocatedOfficer2                   { get; set; }
        public string CreateTasksWorkInstruction2                   { get; set; }
        public string CreateTasksTaskName2                          { get; set; }
        public string CreateTasksComments2                          { get; set; }
        public string CreateTaskDueDate2                            { get; set; }
        public string CreateTaskDoWeNeedUploadFile2                 { get; set; }
        public string CreateTaskFileUpload2                         { get; set; }
        public string CreateTaskTenureSearchItem21                  { get; set; }
        public string CreateTaskTenureSearchItem22                  { get; set; }
        public string CreateTaskTenureSearchItem23                  { get; set; }
        public string CreateTaskTenureSearchItem24                  { get; set; }
        public string CreateTaskTenureSearchItem25                  { get; set; }
        public string FinaliseCase                                  { get; set; }
        public string FinalisationReason                            { get; set; }
        public string FinalisationComments                          { get; set; }


        public static AppSubTestData FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            AppSubTestData appSubTestData = new AppSubTestData
            {
                SerialNumber = values[0],
                UserName = values[1],
                Password = values[2],
                UserFirstName = values[3],
                UserFullName = values[4],
                Role = values[5],
                CaseInitiation = values[6],
                AssociatedCustomer = values[7],
                ApplicantFirstName = values[8],
                ApplicantLastName = values[9],
                IsApplicantTheCustomer = values[10],
                CustomerFirstName = values[11],
                CustomerLastName = values[12],
                ProofOfConsentFileName = values[13],
                RequestType = values[14],
                RequestCategory = values[15],
                RequestDescription = values[16],
                CaseDescription = values[17],
                CLEF = values[18],
                Signed = values[19],
                DateSigned = values[20],
                Position = values[21],
                ProjectName = values[22],
                CaseDueDate = values[23],
                DateReceived = values[24],
                ApplicantRequestDocumentName = values[25],
                EventFromDate = values[26],
                EventFromTime = values[27],
                EventToDate = values[28],
                EventToTime = values[29],
                EventPurpose = values[30],
                EventLandSize = values[31],
                EvenLayoutDocument = values[32],
                AlterationToSite = values[33],
                SitePlanDocument = values[34],
                NoOfVisitors = values[35],
                NoOfEmployees = values[36],
                NoOfArtists = values[37],
                Revenue = values[38],
                Grants = values[39],
                Expenses = values[40],
                GrantDocumentName = values[41],
                IsLandRecordAssociated = values[42],
                LandGeneralInformation = values[43],
                LandMapDoc = values[44],
                LotNumber = values[45],
                PlanNumber = values[46],
                Pin = values[47],
                ReserveNumber = values[48],
                ReserveClass = values[49],
                ReservePurpose = values[50],
                IsLandTitleAvailable = values[51],
                Volume = values[52],
                Folio = values[53],
                PIHName = values[54],
                IsCustomerPIH = values[55],
                StreetNumber = values[56],
                StreetName = values[57],
                StreetType = values[58],
                Suburb = values[59],
                Lga = values[60],
                LandRecordComment = values[61],
                GeneralInfoComment = values[62],
                MultipleLandRecords = values[63],
                PIHConsulted = values[64],
                LgaConsulted = values[65],
                LgaCorrespondenceDocumentName = values[66],
                LgaComments = values[67],
                OtherAgenciesConsultation = values[68],
                AgencyName = values[69],
                Outcome = values[70],
                ExternalAgencyConsultationDoc = values[71],
                AdditionalInfo = values[72],
                AdditionalDocumentName = values[73],
                ApplicantsOwnCaseReference = values[74],
                OwnCaseReferenceNumber = values[75],
                SendSubmissionSummary = values[76],
                MethodOfContact = values[77],
                CopyEmailToCustomer = values[78],
                CopyEmailToOtherRecipient = values[79],
                OtherRecipeientEmail = values[80],
                OtherDocumentsToSend = values[81],
                OtherDocumentName = values[82],
                PrintAndPostDate = values[83],
                ConfirmReadyToEmail = values[84],
                LandRecords = values[85],
                OtherAgenciesConsulted = values[86],
                App_EmailAddress = values[87],
                App_PhoneNumber = values[88],
                App_MobileNumber = values[89],
                App_Organisation = values[90],
                App_ABN = values[91],
                App_ACN = values[92],
                App_ICN = values[93],
                App_Address = values[94],
                App_Country = values[95],
                App_State = values[96],
                App_Suburb = values[97],
                App_PostCode = values[98],
                Cust_EmailAddress = values[99],
                Cust_PhoneNumber = values[100],
                Cust_MobileNumber = values[101],
                Cust_Organisation = values[102],
                Cust_ABN = values[103],
                Cust_ACN = values[104],
                Cust_ICN = values[105],
                Cust_Address = values[106],
                Cust_Country = values[107],
                Cust_State = values[108],
                Cust_Suburb = values[109],
                Cust_PostCode = values[110],
                EndPoint = values[111],
                CaseAllocation = values[112],
                CaseAllocationManager = values[113],
                CaseAllocationManagerPassword = values[114],
                CaseAllocationAction = values[115],
                CaseAllocationWI = values[116],
                CaseAllocationComments = values[117],
                CaseAllocationDueDate = values[118],
                ProceedWithCase = values[119],
                PriorityRating = values[120],
                PriorityRatingWI = values[121],
                PriorityRatingOption = values[122],
                PriorityRatingManagerDecision = values[123],
                PriorityRatingManagerOption = values[124],
                PriorityRatingManagerComments = values[125],
                Integration = values[126],
                IntegrationFileNumber = values[127],
                IntegrateWithObjectiveComments = values[128],
                CreateTask = values[129],
                NoOfCreateTasks = values[130],
                CreateTaskAllocatedOfficer1 = values[131],
                CreateTasksWorkInstruction1 = values[132],
                CreateTasksTaskName1 = values[133],
                CreateTasksComments1 = values[134],
                CreateTaskDueDate1 = values[135],
                CreateTaskDoWeNeedUploadFile1 = values[136],
                CreateTaskFileUpload1 = values[137],
                CreateTaskTenureSearchItem11 = values[138],
                CreateTaskTenureSearchItem12 = values[139],
                CreateTaskTenureSearchItem13 = values[140],
                CreateTaskTenureSearchItem14 = values[141],
                CreateTaskTenureSearchItem15 = values[142],
                CreateTaskAllocatedOfficer2 = values[143],
                CreateTasksWorkInstruction2 = values[144],
                CreateTasksTaskName2 = values[145],
                CreateTasksComments2 = values[146],
                CreateTaskDueDate2 = values[147],
                CreateTaskDoWeNeedUploadFile2 = values[148],
                CreateTaskFileUpload2 = values[149],
                CreateTaskTenureSearchItem21 = values[150],
                CreateTaskTenureSearchItem22 = values[151],
                CreateTaskTenureSearchItem23 = values[152],
                CreateTaskTenureSearchItem24 = values[153],
                CreateTaskTenureSearchItem25 = values[154],
                FinaliseCase = values[155],
                FinalisationReason = values[156],
                FinalisationComments = values[157]
            };
            return appSubTestData;
        }

        public static List<AppSubTestData>  GetAppSubTestData(string testDataFile)
        {
            string filepath = BasePage.GetFolderPathInProjectRoot("Resources") + testDataFile;
            filepath = Path.GetFullPath(@filepath);

            if (!File.Exists(filepath))
                throw new Exception("App Sub Test Data file does not exist");
            
            List<AppSubTestData> values = File.ReadAllLines(filepath)
                               .Skip(1)
                               .Select(v => AppSubTestData.FromCsv(v))
                               .ToList();
            return values;
        }
    }
}
