﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.Resources;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using LandsWa.Acceptance.Smoke.Tests.Training;
using log4net.Config;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Tests
{
    class TrainingDataCreation : BaseTest
    {
        [Test]
        public void CreateTrainingData()
        {
            StartDataRowNumber = 10 - ArrayCsvIndexOffsetValue; //Array index = CSV Line Number - 2
            EndDataRowNumber = 10 - ArrayCsvIndexOffsetValue; //Array index = CSV Line Number - 2         
            for (; StartDataRowNumber <= EndDataRowNumber; StartDataRowNumber++)
            {
                try
                {
                    row = AppSubTestDataList[StartDataRowNumber];
                    Console.WriteLine("############### NEW ROW #############################");
                    WorkFlowHelper.CreateWorkFlow(row);
                    BasePage.CaseNumber = "NOT ASSIGNED";
                }
                catch (Exception e)
                {
                    using (var tw = new StreamWriter(failedCasesFilePath, true))
                    {
                        tw.WriteLine($"{row.UserName},{BasePage.CaseNumber}");
                    }
                    Console.WriteLine(e.Message);
                    Logger.WriteLog($"CASE CREATION FAILED FOR ROW - {row.SerialNumber}; Case NUMBER {BasePage.CaseNumber}");
                    Logger.WriteLog(e.Message);
                    Logger.WriteLog(e.StackTrace);
                    {
                        int endIndex = 0; ;
                        string path = BasePage.GetFolderPathInProjectRoot("ss");
                        if (TestContext.CurrentContext.Test.Name.Length > 50)
                            endIndex = 50;
                        else
                            endIndex = TestContext.CurrentContext.Test.Name.Length;
                        string method = $"{row.SerialNumber}_{row.UserFirstName}_Failed";
                        path = $@"{path}{method}.png";
                        BasePage.TakeScreenshot(path);
                        Logger.WriteLog($"Screenshot taken: {path}");
                    }
                }
            }
        }

       
    }
}
