﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests
{
    public class LandRecordTestData
    {
        public string LandRecordSerialNo { get; set; }
        public string LotNumber                 { get; set; }
        public string PlanNumber                { get; set; }
        public string Pin                       { get; set; }
        public string ReserveNumber             { get; set; }
        public string ReserveClass              { get; set; }
        public string ReservePurpose            { get; set; }
        public string IsLandTitleAvailable      { get; set; }
        public string Volume                    { get; set; }
        public string Folio                     { get; set; }
        public string PIHName                   { get; set; }
        public string IsCustomerPIH             { get; set; }
        public string StreetNumber              { get; set; }
        public string StreetName                { get; set; }
        public string StreetType                { get; set; }
        public string Suburb                    { get; set; }
        public string Lga                       { get; set; }
        public string LandRecordComment         { get; set; }
        public string LandGeneralInfoComment    { get; set; }
        public string LandMapDoc                { get; set; }

        public static LandRecordTestData FromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            LandRecordTestData landRecordData = new LandRecordTestData
            {
                LandRecordSerialNo = values[0],
                LotNumber = values[1],
                PlanNumber = values[2],
                Pin = values[3],
                ReserveNumber = values[4],
                ReserveClass = values[5],
                ReservePurpose = values[6],
                IsLandTitleAvailable = values[7],
                Volume = values[8],
                Folio = values[9],
                PIHName = values[10],
                IsCustomerPIH = values[11],
                StreetNumber = values[12],
                StreetName = values[13],
                StreetType = values[14],
                Suburb = values[15],
                Lga = values[16],
                LandRecordComment = values[17],
                LandGeneralInfoComment = values[18],
                LandMapDoc = values[19]
            };
            return landRecordData;
        }

        public static List<LandRecordTestData> GetLandRecordTestData(string testDataFile)
        {
            string filepath = BasePage.GetFolderPathInProjectRoot("Resources") + testDataFile;
            filepath = Path.GetFullPath(@filepath);

            if (!File.Exists(filepath))
                throw new Exception("App Sub Test Data file does not exist");

            List<LandRecordTestData> values = File.ReadAllLines(filepath)
                               .Skip(1)
                               .Select(v => LandRecordTestData.FromCsv(v))
                               .ToList();
            return values;
        }
    }
}
