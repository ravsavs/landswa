﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using log4net;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Resources
{
    class CaseSubmission
    {
        public static dynamic Login(LoginPage loginPage, AppSubTestData dataRow)
        {
            var dashboard = loginPage.LogIn(dataRow.UserName, dataRow.Password);
            return dashboard;
        }

        public static dynamic SelectCaseInitiation(MyDashboardPage myDashboard, AppSubTestData dataRow)
        {
            var page = myDashboard.ClickCreateNewCaseButton()
                .InternalOrExternal(dataRow)
                .ClickDoneButton();
            return page;
        }

        public static ApplicantDetailsMileStone SearchAndSelectApplicant(dynamic page, AppSubTestData dataRow)
        {
            ApplicantDetailsMileStone applicantDetailsPage = null;
            AssignApplicantCustomerPage assignApplicantCustomerPage = null;

            if (BasePage.caseType == CaseType.External)
            {
                assignApplicantCustomerPage = (AssignApplicantCustomerPage)page;
                assignApplicantCustomerPage.SearchAnApplicantWithName(dataRow.ApplicantFirstName, dataRow.ApplicantLastName)
                    .SelectTheApplicantFromSearchResultWithName(dataRow.ApplicantFirstName);
                if (dataRow.IsApplicantTheCustomer.ToLower() == "no")
                    assignApplicantCustomerPage.CheckApplicantIsNotCustomer()
                        .SearchACustomerWithName(dataRow.CustomerFirstName, dataRow.CustomerLastName)
                        .SelectTheCustomerFromSearchResultWithName(dataRow.CustomerFirstName);
                applicantDetailsPage = assignApplicantCustomerPage.Continue();
            }
            if (BasePage.caseType == CaseType.Internal && BasePage.customerAssociatedWithCase == CustomerAssociatedWithCase.Yes)
            {
                assignApplicantCustomerPage = (AssignApplicantCustomerPage)page;
                assignApplicantCustomerPage.SearchACustomerWithName(dataRow.CustomerFirstName, dataRow.CustomerLastName)
                        .SelectTheCustomerFromSearchResultWithName(dataRow.CustomerFirstName);
                applicantDetailsPage = assignApplicantCustomerPage.Continue();
            }
            if (BasePage.caseType == CaseType.Internal && BasePage.customerAssociatedWithCase == CustomerAssociatedWithCase.No)
                applicantDetailsPage = (ApplicantDetailsMileStone)page;

            return applicantDetailsPage;
        }

        internal static void LogInputs(ILog log, AppSubTestData row)
        {
            log.Info($"INITITATING ROW NO {row.SerialNumber} EXECUTION");
            Logger.WriteLog(row);
        }

        public static RequestDetailsMileStone GoToRequestDetailsPage(ApplicantDetailsMileStone applicantDetailsPage)
        {
            return applicantDetailsPage.ClickContinueButton();
        }

        public static LandDetailsMileStone EnterRequestDetails(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            LandDetailsMileStone landDetailsPage = null;
            if (BasePage.caseType == CaseType.External)
                switch (dataRow.RequestType.ToLower())
                {
                    case "general":
                        BasePage.requestType = RequestType.General;
                        landDetailsPage = AddGeneralRequestType(requestDetailsPage, dataRow);
                        break;
                    case "event":
                        BasePage.requestType = RequestType.Event;
                        landDetailsPage = AddEventRequestType(requestDetailsPage, dataRow);
                        break;
                    case "lga":
                        BasePage.requestType = RequestType.LGA;
                        landDetailsPage = AddLgaRequestType(requestDetailsPage, dataRow);
                        break;
                    default:
                        Logger.WriteLog("REQUEST TYPE DOES NOT EXIST - INCORRECT REQUEST TYPE ENTERED");
                        break;
                }
            else if (BasePage.caseType == CaseType.Internal)
            {
                if (BasePage.customerAssociatedWithCase == CustomerAssociatedWithCase.No)
                    landDetailsPage = AddRequestDetailsForInternalCaseWithNoCust(requestDetailsPage, dataRow);
                else if (BasePage.customerAssociatedWithCase == CustomerAssociatedWithCase.Yes)
                    landDetailsPage = AddRequestDetailsForInternalCaseWithCust(requestDetailsPage, dataRow);
            }
            return landDetailsPage;
        }

        private static LandDetailsMileStone AddGeneralRequestType(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            var landRecordsPage = (LandDetailsMileStone)requestDetailsPage.SelectGeneralRequestType()
                .SelectCategoryFromDropdown(dataRow.RequestCategory)
                .EnterDescription(dataRow.RequestDescription)
                .ClickCLEFRequestCheckbox(dataRow.CLEF)
                .ClickApplicantSignedCheckbox(dataRow.Signed)
                .EnterDateSigned(dataRow.DateSigned)
                .EnterProject(dataRow.ProjectName)
                .EnterDateReceived(dataRow.DateReceived)
                .UploadDocument(dataRow.ApplicantRequestDocumentName)
                .ClickContinueButton();
            return landRecordsPage;
        }

        private static LandDetailsMileStone AddEventRequestType(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            var landRecordsPage = (LandDetailsMileStone)requestDetailsPage.SelectEventRequestType()
                .CompleteEventDetails(dataRow)
                .EnterProject(dataRow.ProjectName)
                .ClickContinueButton();

            return landRecordsPage;
        }

        private static LandDetailsMileStone AddLgaRequestType(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            var landDetailsPage = (LandDetailsMileStone)requestDetailsPage.SelectLgaRequestType()
                .SelectCategoryFromDropdown(dataRow.RequestCategory)
                .EnterDescription(dataRow.RequestDescription)
                .ClickCLEFRequestCheckbox()
                .ClickApplicantSignedCheckbox(dataRow.Signed)
                .EnterDateSigned(dataRow.DateSigned)
                .EnterPosition(dataRow.Position)
                .EnterProject(dataRow.ProjectName)
                .EnterCaseDueDate(dataRow.CaseDueDate)
                .EnterDateReceived(dataRow.DateReceived)
                .UploadDocument(dataRow.ApplicantRequestDocumentName)
                .ClickContinueButton();

            return landDetailsPage;
        }

        private static LandDetailsMileStone AddRequestDetailsForInternalCaseWithNoCust(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            var landDetailsPage = (LandDetailsMileStone)requestDetailsPage
                .SelectCategoryFromDropdown(dataRow.RequestCategory)
                .EnterDescription(dataRow.RequestDescription)
                .ClickApplicantSignedCheckbox(dataRow.Signed)
                .EnterDateSigned(dataRow.DateSigned)
                .EnterProject(dataRow.ProjectName)
                .EnterCaseDueDate(dataRow.CaseDueDate)
                .EnterDateReceived(dataRow.DateReceived)
                .UploadDocument(dataRow.ApplicantRequestDocumentName)
                .ClickContinueButton();

            return landDetailsPage;
        }
        
        private static LandDetailsMileStone AddRequestDetailsForInternalCaseWithCust(RequestDetailsMileStone requestDetailsPage, AppSubTestData dataRow)
        {
            var landDetailsPage = (LandDetailsMileStone)requestDetailsPage
                .SelectCategoryFromDropdown(dataRow.RequestCategory)
                .EnterDescription(dataRow.RequestDescription)
                .EnterCaseDescription(dataRow.CaseDescription)
                .ClickApplicantSignedCheckbox(dataRow.Signed)
                .EnterDateSigned(dataRow.DateSigned)
                .EnterProject(dataRow.ProjectName)
                .EnterCaseDueDate(dataRow.CaseDueDate)
                .EnterDateReceived(dataRow.DateReceived)
                .UploadDocument(dataRow.ApplicantRequestDocumentName)
                .ClickContinueButton();

            return landDetailsPage;
        }

        internal static ConsultationMileStone EnterLandDetails(LandDetailsMileStone landDetailsPage, AppSubTestData dataRow, List<LandRecordTestData> landRecordData)
        {
            ConsultationMileStone page = null;
            LandDetailsMileStone landDetails = null;
            switch (BasePage.caseType)
            {
                case CaseType.Internal:
                    switch (dataRow.IsLandRecordAssociated.ToLower())
                    {
                        case ("yes"):
                            landDetails = landDetailsPage.AreLandRecordsAssociatedWithThisInternalCase(IsLandRecordAssociatedWithThisInternalCase.Yes);
                            landDetails = AddLandRecord(dataRow, landDetailsPage, landRecordData);
                            Logger.WriteLog("Added a Land Record");
                            landDetails = landDetails.EnterGeneralInformation(dataRow.LandGeneralInformation);
                            Logger.WriteLog($"Added General Information \"{dataRow.LandGeneralInformation}\" on Land Details Page");
                            landDetails = landDetails.UploadMapDocument(dataRow.LandMapDoc);
                            Logger.WriteLog("Added a Land Map Doc");
                            page = landDetails.ClickContinueButton();
                            Logger.WriteLog("Click Continue button on Land Records Page");
                            break;
                        case ("no"):
                            landDetails = landDetailsPage.AreLandRecordsAssociatedWithThisInternalCase(IsLandRecordAssociatedWithThisInternalCase.No);
                            Logger.WriteLog("No Land Record associated with this case");
                            landDetails.EnterGeneralInformation(dataRow.LandGeneralInformation);
                            Logger.WriteLog($"Added General Information \"{dataRow.LandGeneralInformation}\" on Land Details Page");
                            landDetails.UploadMapDocument(dataRow.LandMapDoc);
                            Logger.WriteLog("Added a Land Map Doc");
                            page = landDetails.ClickContinueButton();
                            Logger.WriteLog("Click Continue button on Land Records Page");
                            break;
                        default:
                            break;
                    }
                    break;
                case CaseType.External:
                    landDetails = AddLandRecord(dataRow, landDetailsPage, landRecordData);
                    Logger.WriteLog("Added a Land Record");
                    landDetails = landDetails.EnterGeneralInformation(dataRow.LandGeneralInformation);
                    Logger.WriteLog($"Added General Information \"{dataRow.LandGeneralInformation}\" on Land Details Page");
                    landDetails = landDetails.UploadMapDocument(dataRow.LandMapDoc);
                    Logger.WriteLog("Added a Land Map Doc");
                    page = landDetails.ClickContinueButton();
                    Logger.WriteLog("Click Continue button on Land Records Page");
                    break;
            }
            return page;
            
        }

        private static LandDetailsMileStone AddLandRecord(AppSubTestData dataRow, LandDetailsMileStone page, List<LandRecordTestData> landRecordData) //Use this function to add multiple land records
        {
            Decision decision = Decision.NotApplicable;
            if (dataRow.IsLandTitleAvailable.ToLower() == "yes")
                decision = Decision.Yes;
            else if (dataRow.IsLandTitleAvailable.ToLower() == "no")
                decision = Decision.No;

            var landDetailsPage =  page.AddALandRecord(
                dataRow.LotNumber, dataRow.PlanNumber, dataRow.Pin, dataRow.ReserveNumber,
                dataRow.ReserveClass, dataRow.ReservePurpose, decision, dataRow.StreetNumber,
                dataRow.StreetName, dataRow.StreetType, dataRow.Suburb, dataRow.Lga, dataRow.LandRecordComment,
                dataRow.Volume, dataRow.Folio, "20181010 LGA Letter.docx", dataRow.PIHName, dataRow.IsCustomerPIH
                );

            if (dataRow.MultipleLandRecords.ToLower() == "yes")
                AddMultipleLandRecord(page, landRecordData);

            return landDetailsPage;
        }

        private static LandDetailsMileStone AddMultipleLandRecord(LandDetailsMileStone page, List<LandRecordTestData> landRecordData) //Use this function to add multiple land records
        {
            LandDetailsMileStone landDetailsPage = null;
            foreach (LandRecordTestData dataRow in landRecordData)
            {
                Decision decision = Decision.NotApplicable;
                if (dataRow.IsLandTitleAvailable.ToLower() == "yes")
                    decision = Decision.Yes;
                else if (dataRow.IsLandTitleAvailable.ToLower() == "no")
                    decision = Decision.No;

                landDetailsPage = page.AddALandRecord(
                    dataRow.LotNumber, dataRow.PlanNumber, dataRow.Pin, dataRow.ReserveNumber,
                    dataRow.ReserveClass, dataRow.ReservePurpose, decision, dataRow.StreetNumber,
                    dataRow.StreetName, dataRow.StreetType, dataRow.Suburb, dataRow.Lga, dataRow.LandRecordComment,
                    dataRow.Volume, dataRow.Folio, "20181010 LGA Letter.docx", dataRow.PIHName, dataRow.IsCustomerPIH
                    );
            }
            return landDetailsPage;
        }

        internal static AdditionalInformationMileStone EnterConsultationDetails(ConsultationMileStone consultationPage, AppSubTestData dataRow)
        {
            AdditionalInformationMileStone additionalInfo = null;
            Logger.WriteLog("Begin with Consultation Page");
            switch (BasePage.caseType)
            {
                case CaseType.Internal:
                    if (BasePage.AppHasCustomer == true && BasePage.IsLandTitleAvailable == true && BasePage.PIHExists == true)
                        consultationPage = consultationPage.ClickPIHCheckboxToConsult(dataRow.PIHName)
                            .ClickHasPIHbeenConsultedRadioButton(dataRow.PIHConsulted)
                            .ClickUpdateButton()
                            .ClickLGACheckboxToConsult(BasePage.LGAName)
                            .HasLGABeenConsultedRadioButtonResponse(dataRow.LgaConsulted)
                            .ClickUpdateButton();
                    if (dataRow.OtherAgenciesConsultation.ToLower() == "yes")
                        consultationPage = consultationPage
                            .AreOtherAgenciesConsultedWithThisInternalCase(dataRow.OtherAgenciesConsultation)
                            .ClickOnAddOtherAgencyImage()
                            .AddOtherAgency(dataRow.AgencyName)
                            .EnterOutcome("Approved")
                            .UploadCorrespondanceDocument(dataRow.ExternalAgencyConsultationDoc)
                            .ClickUpdateButton();
                    else if (dataRow.OtherAgenciesConsultation.ToLower() == "no")
                        consultationPage = consultationPage
                            .AreOtherAgenciesConsultedWithThisInternalCase(dataRow.OtherAgenciesConsultation);
                    break;
                case CaseType.External:
                    if (BasePage.LGAName != null || BasePage.LGAName != "")
                    {
                        if (BasePage.PIHExists && BasePage.requestType == RequestType.General)
                            consultationPage = consultationPage
                                .ClickPIHCheckboxToConsult(dataRow.PIHName)
                                .ClickHasPIHbeenConsultedRadioButton(dataRow.PIHConsulted)
                                .ClickUpdateButton();

                        consultationPage = consultationPage.ClickLGACheckboxToConsult(BasePage.LGAName)
                                .HasLGABeenConsultedRadioButtonResponse(dataRow.LgaConsulted)
                                .UploadDocument(dataRow.LgaCorrespondenceDocumentName)
                                .EnterCommentsReceivedFromLga(dataRow.LgaComments)
                                .ClickUpdateButton();
                    }
                    break;
            }
            Thread.Sleep(1000);
            additionalInfo = consultationPage.ClickContinueButton();
            Logger.WriteLog("Finish with Consultation Page");
            return additionalInfo;
        }

        internal static ReviewMileStone EnterAdditionalInfo(AdditionalInformationMileStone additionalInfoPage, AppSubTestData dataRow)
        {
            Logger.WriteLog("Begin with Additional Info Page");
            ReviewMileStone reviewPage = null;
            additionalInfoPage = additionalInfoPage
                .EnterAdditionalInfo(dataRow.AdditionalInfo)
                .UploadDocument(dataRow.AdditionalDocumentName);

            if (dataRow.OwnCaseReferenceNumber != "" && dataRow.ApplicantsOwnCaseReference.ToLower() == "yes")
                additionalInfoPage = additionalInfoPage
                    .HasTheApplicantProvidedOwnCaseNo(dataRow.ApplicantsOwnCaseReference)
                    .EnterCaseReferenceNo(dataRow.OwnCaseReferenceNumber);

            reviewPage = additionalInfoPage.ClickContinueButton();
            Logger.WriteLog("Finish with Additional Info Page");
            return reviewPage;

        }

        internal static SubmissionMileStone ReviewAppSub(ReviewMileStone reviewPage, AppSubTestData dataRow)
        {
            Logger.WriteLog("Begin with Review Page");
            var page = reviewPage.ClickCheckBox().ClickSubmitButton();
            Logger.WriteLog("Finish with Review Page");
            return page;
        }

        internal static MyDashboardPage CompleteAppSub(SubmissionMileStone submissionPage, AppSubTestData dataRow)
        {
            Logger.WriteLog("Begin with Case Submission Page");
            MyDashboardPage myDashboardPage = null;

            if (BasePage.caseType == CaseType.Internal && dataRow.AssociatedCustomer.ToLower() == "no")
                myDashboardPage = submissionPage
                    .ClickDoneButton();
            else if (dataRow.SendSubmissionSummary.ToLower() == "yes")
            {
                submissionPage = submissionPage.SendCaseSummaryToApplicantRadioButton(dataRow.SendSubmissionSummary);

                if (dataRow.App_EmailAddress != "" && dataRow.App_Address != "")
                    submissionPage.SelectMethodOfContact(dataRow.MethodOfContact);

                myDashboardPage = submissionPage.CopyEmailToCustomer(dataRow.CopyEmailToCustomer)
                        .CopyEmailToOtherRecipients(dataRow.CopyEmailToOtherRecipient, dataRow.OtherRecipeientEmail)
                        .AnyOtherDocumentsToSend(dataRow.OtherDocumentsToSend, dataRow.OtherDocumentName)
                        .EnterTheDatePrintAndPostDocumentWereSent(dataRow.PrintAndPostDate)
                        .ClickReadyToEmailConfirmationCheckbox()
                        .ClickDoneButton();
            }
            else if (dataRow.SendSubmissionSummary.ToLower() == "no")
                myDashboardPage = submissionPage.SendCaseSummaryToApplicantRadioButton(dataRow.SendSubmissionSummary)
                    .ClickDoneButton();
            while (!myDashboardPage.IsOfficerNameDisplayed(dataRow.UserFirstName)) ;
            Logger.WriteLog("Finish with Case Submission Page");
            return myDashboardPage;

        }
    }
}
