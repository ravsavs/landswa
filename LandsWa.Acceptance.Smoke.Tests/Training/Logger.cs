﻿using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandsWa.Acceptance.Smoke.Tests.Resources
{
    public class Logger
    {

        public static void WriteLog(AppSubTestData row)
        {
            BaseTest.log.Info(
                $"{row.SerialNumber},{row.UserName},{row.Password},{row.UserFirstName},{row.Role},{row.CaseInitiation}," +
                $"{ row.AssociatedCustomer},{ row.ApplicantFirstName},{row.ApplicantLastName},{ row.IsApplicantTheCustomer}," +
                $"{ row.CustomerFirstName},{row.CustomerLastName},{ row.ProofOfConsentFileName}," +
                $"{ row.RequestType},{ row.RequestCategory},{ row.RequestDescription},{ row.CaseDescription},{ row.CLEF}," +
                $"{ row.Signed},{ row.DateSigned},{ row.Position},{ row.ProjectName},{ row.CaseDueDate},{ row.DateReceived}," +
                $"{ row.ApplicantRequestDocumentName},{ row.EventFromDate},{ row.EventFromTime},{ row.EventToDate},{ row.EventToTime}," +
                $"{ row.EventPurpose},{ row.EventLandSize},{ row.EvenLayoutDocument},{ row.AlterationToSite},{ row.SitePlanDocument}," +
                $"{ row.NoOfVisitors},{ row.NoOfEmployees},{ row.NoOfArtists},{ row.Revenue},{ row.Grants},{ row.Expenses},{ row.GrantDocumentName}," +
                $"{ row.IsLandRecordAssociated},{ row.LandGeneralInformation},{ row.LandMapDoc},{ row.LotNumber},{ row.PlanNumber}," +
                $"{ row.Pin},{ row.ReserveNumber},{ row.ReserveClass},{ row.ReservePurpose},{ row.IsLandTitleAvailable},{ row.Volume}," +
                $"{ row.Folio},{ row.PIHName},{ row.IsCustomerPIH},{ row.StreetNumber},{ row.StreetName},{ row.StreetType},{ row.Suburb}," +
                $"{ row.Lga},{ row.LandRecordComment},{ row.GeneralInfoComment},{ row.MultipleLandRecords},{ row.LgaConsulted}," +
                $"{ row.LgaCorrespondenceDocumentName},{ row.LgaComments},{ row.OtherAgenciesConsultation},{ row.AgencyName}," +
                $"{ row.Outcome},{ row.ExternalAgencyConsultationDoc},{ row.AdditionalInfo},{ row.AdditionalDocumentName}," +
                $"{ row.ApplicantsOwnCaseReference},{ row.OwnCaseReferenceNumber},{ row.SendSubmissionSummary},{ row.MethodOfContact}," +
                $"{ row.CopyEmailToCustomer},{ row.CopyEmailToOtherRecipient},{ row.OtherRecipeientEmail},{ row.OtherDocumentsToSend}," +
                $"{ row.OtherDocumentName},{ row.PrintAndPostDate},{ row.ConfirmReadyToEmail},{ row.LandRecords}," +
                $"{ row.OtherAgenciesConsulted},{ row.App_EmailAddress},{ row.App_PhoneNumber},{ row.App_MobileNumber},{ row.App_Organisation}," +
                $"{ row.App_ABN},{ row.App_ACN},{ row.App_ICN},{ row.App_Address},{ row.App_Country},{ row.App_State},{ row.App_Suburb}," +
                $"{ row.App_PostCode},{ row.Cust_EmailAddress},{ row.Cust_PhoneNumber},{ row.Cust_MobileNumber},{ row.Cust_Organisation}," +
                $"{ row.Cust_ABN},{ row.Cust_ACN},{ row.Cust_ICN},{ row.Cust_Address},{ row.Cust_Country},{ row.Cust_State}," +
                $"{ row.Cust_Suburb},{ row.Cust_PostCode},{ row.EndPoint}");
        }

        public static void WriteLog(string text)
        {
            BaseTest.log.Info(text);
        }
    }
}
