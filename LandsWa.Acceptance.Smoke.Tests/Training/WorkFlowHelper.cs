﻿using LandsWa.Acceptance.Smoke.Tests.Pages;
using LandsWa.Acceptance.Smoke.Tests.Resources;
using LandsWa.Acceptance.Smoke.Tests.SetupTeardown;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LandsWa.Acceptance.Smoke.Tests.Helper.Enumerations;

namespace LandsWa.Acceptance.Smoke.Tests.Training
{
    public class TestWorkFlowHelper : BaseTest
    {
        private IWebDriver _driver = null;

        public TestWorkFlowHelper(IWebDriver driver)
        {
            _driver = driver;
        }

        public void CreateWorkFlow(AppSubTestData csvDataRow)
        {
            Initialise(csvDataRow);
            SubmitNewCase(csvDataRow, LandRecordTestDataList);
            NewCaseAllocation(csvDataRow);
            ProceedWithCase(csvDataRow);
            PriorityRating(csvDataRow);
            AmendOrApprovePriorityRating(csvDataRow);
            IntegrationWithObjective(csvDataRow);
            CreateTask(csvDataRow);
            FinaliseCase(csvDataRow);
        }

        public void Initialise(AppSubTestData csvDataRow)
        {
            Logger.WriteLog("---------------New Case Creation Inititated------------------");
            BasePage.ApplicantPostalAddressExists = false;
            BasePage.AppHasCustomer = false;
            BasePage.IsLandTitleAvailable = false;
            BasePage.PIHExists = false;
            CaseSubmission.LogInputs(log, csvDataRow);
        }

        public void SubmitNewCase(AppSubTestData submissionData, List<LandRecordTestData> landRecordTestDataList)
        {
            int step = 0, counter = 0;
            Logger.WriteLog("Case Submission STARTED");
            _driver.Manage().Cookies.DeleteAllCookies();
            BasePage.firstTimeLoggedIn = true;
            loginPage = new LoginPage(_driver);
            myDashboard = CaseSubmission.Login(loginPage, submissionData);
            var page = CaseSubmission.SelectCaseInitiation(myDashboard, submissionData);
            do
            {
                try
                {
                    if (step == 0)
                    {
                        applicantDetailsPage = CaseSubmission.SearchAndSelectApplicant(page, submissionData);
                        step++;
                        Logger.WriteLog($"Applicants Postal Address : {BasePage.ApplicantPostalAddressExists.ToString()}");
                        Logger.WriteLog($"Case has an associated Customer : {BasePage.AppHasCustomer.ToString()}");
                    }

                    if (step == 1)
                    {
                        requestDetailsPage = CaseSubmission.GoToRequestDetailsPage(applicantDetailsPage);
                        step++;
                        Logger.WriteLog($"New Case Number : {BasePage.CaseNumber}");
                    }

                    if (step == 2)
                    {
                        landDetailsPage = CaseSubmission.EnterRequestDetails(requestDetailsPage, submissionData);
                        step++;
                    }
                    if (step == 3)
                    {
                        consultationPage = CaseSubmission.EnterLandDetails(landDetailsPage, submissionData, landRecordTestDataList);
                        step++;
                        Logger.WriteLog($"Is Land Title Available : {BasePage.IsLandTitleAvailable.ToString()}");
                        Logger.WriteLog($"Does PIH exist? : {BasePage.PIHExists.ToString()}");
                        Logger.WriteLog($"LGA Name : {BasePage.LGAName}");
                    }

                    if (step == 4)
                    {
                        additionalInfoPage = CaseSubmission.EnterConsultationDetails(consultationPage, submissionData);
                        step++;
                    }

                    if (step == 5)
                    {
                        reviewPage = CaseSubmission.EnterAdditionalInfo(additionalInfoPage, submissionData);
                        step++;
                    }

                    if (step == 6)
                    {
                        submissionPage = CaseSubmission.ReviewAppSub(reviewPage, submissionData);
                        step++;
                    }

                    if (step == 7)
                    {
                        myDashboard = CaseSubmission.CompleteAppSub(submissionPage, submissionData);
                        step++;
                    }
                }
                catch
                {
                    _driver.Navigate().Refresh();
                    _driver.SwitchTo().Alert().Accept();
                    myDashboard.ClickMYDashboardLink()
                        .FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                    caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
                    caseDashboard.ClickFirstTaskInList();
                }
                counter++;
            } while (step < 8 && counter < 2);

            if (step == 8)
            {
                loginPage = myDashboard.LogOut();
                Logger.WriteLog("Case Submission FINISHED");
            }
            else if (step < 8)
            {
                Logger.WriteLog("Case Submission FAILED*****");
                throw new Exception("Case Submission Failed");
            }
        }
        public void NewCaseAllocation(AppSubTestData allocationData)
        {
            if (allocationData.CaseAllocation.ToLower() == "yes")
            {
                Logger.WriteLog("New Case Allocation STARTED");
                myDashboard = loginPage.LogIn(allocationData.CaseAllocationManager, allocationData.CaseAllocationManagerPassword);
                teamDashboard = myDashboard.NavigateToTeamDashboard().FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
                allocateNewCase = (AllocateNewCasePage)caseDashboard.ClickTaskInOpenTasksGrid("Allocate New Case");
                caseDashboard = allocateNewCase.ClickAcceptTask()
                    .SelectCaseAllocationAction(allocationData.CaseAllocationAction)
                    .EnterOfficerName(allocationData.UserFullName)
                    .EnterWI(allocationData.CaseAllocationWI)
                    .EnterComments(allocationData.CaseAllocationComments)
                    .EnterDueDate(allocationData.CaseAllocationDueDate)
                    .ClickDoneButton();

                BasePage.NewCaseAllocationTaskCreated = true;

                loginPage = myDashboard.LogOut();
                Logger.WriteLog($"New Case Allocation Data - ");
                Logger.WriteLog($"Perform New Case Allocation?:{allocationData.CaseAllocation}; ");
                Logger.WriteLog($"CaseAllocation:{allocationData.CaseAllocationAction}; ");
                Logger.WriteLog($"OfficerAllocatedTo:{allocationData.UserFullName}; ");
                Logger.WriteLog($"WI:{allocationData.CaseAllocationWI};");
                Logger.WriteLog($"Comments:{allocationData.CaseAllocationComments}; ");
                Logger.WriteLog($"DueDate:{allocationData.CaseAllocationDueDate};");
                Logger.WriteLog("New Case Allocation FINISHED");
            }
        }
        public void ProceedWithCase(AppSubTestData proceedWithCaseData)
        {
            if (proceedWithCaseData.ProceedWithCase.ToLower() == "yes" && BasePage.NewCaseAllocationTaskCreated == true)
            {
                Logger.WriteLog("Proceed With Case Task STARTED");
                myDashboard = loginPage.LogIn(proceedWithCaseData.UserName, proceedWithCaseData.Password);
                myDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
                proceedWithCase = (ProceedWithCasePage)caseDashboard.ClickTaskInOpenTasksGrid("Proceed With Case");
                caseDashboard = proceedWithCase.ClickConfirmationCheckbox()
                    .ClickDoneButton();

                loginPage = myDashboard.LogOut();
            }
            Logger.WriteLog($"Execute Proceed With Case Task?:{proceedWithCaseData.ProceedWithCase};");
            Logger.WriteLog("Proceed With Case Task FINISHED");

        }
        public void PriorityRating(AppSubTestData priorityRatingData)
        {
            if (priorityRatingData.PriorityRating.ToLower() == "yes")
            {
                Logger.WriteLog("Priority Rating Task STARTED");
                myDashboard = loginPage.LogIn(priorityRatingData.UserName, priorityRatingData.Password);

                myDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);

                calculatePriorityRatingPage = (CalculatePriorityRatingPage)caseDashboard
                    .ClickRelatedActionFromRelatedActionTab(RelatedActions.CalculatePriorityRating);

                caseDashboard = calculatePriorityRatingPage.EnterWI(priorityRatingData.PriorityRatingWI)
                    .SelectPriorityOption(priorityRatingData.PriorityRatingOption)
                    .ClickDoneButton()
                    .ClickSummaryTab();
                Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard("Approve or Amend Priority Rating"));
                loginPage = caseDashboard.LogOut();
                Logger.WriteLog("Priority Rating Data - ");
                Logger.WriteLog($"Calculate Priority Rating?:{priorityRatingData.PriorityRating}");
                Logger.WriteLog($"Priority Rating Work Instruction:{priorityRatingData.PriorityRatingWI}, ");
                Logger.WriteLog($"Priority Rating Option:{priorityRatingData.PriorityRatingOption};");
                Logger.WriteLog("Priority Rating Task FINISHED");
            }
        }

        public void AmendOrApprovePriorityRating(AppSubTestData priorityRatingData)
        {
            if (priorityRatingData.PriorityRatingManagerDecision != "")
            {
                Logger.WriteLog("Priority Rating Amend Or Approve STARTED ");
                myDashboard = loginPage.LogIn(priorityRatingData.CaseAllocationManager, priorityRatingData.CaseAllocationManagerPassword);

                teamDashboard = myDashboard.NavigateToTeamDashboard();
                teamDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);
                Assert.IsTrue(caseDashboard.IsNewTaskPresentOnCaseDashboard("Approve or Amend Priority Rating"));

                approveOrAmendPriorityRatingPage = (ApproveOrAmendPriorityRatingPage)caseDashboard
                    .ClickTaskInOpenTasksGrid("Approve or Amend Priority Rating");

                approveOrAmendPriorityRatingPage.ClickAcceptTask()
                    .SelectPriorityRatingDecision(priorityRatingData.PriorityRatingManagerDecision)
                    .EnterManagerComments(priorityRatingData.PriorityRatingManagerComments);

                if (priorityRatingData.PriorityRatingManagerDecision.ToLower().Contains("amend"))
                    approveOrAmendPriorityRatingPage.EditPriorityRating(priorityRatingData.PriorityRatingManagerOption);

                caseDashboard = approveOrAmendPriorityRatingPage.ClickDoneButton();
                caseDashboard.LogOut();

                Logger.WriteLog("Priority Rating Amend or Approve Data - ");
                Logger.WriteLog($"Priority Rating Manager:{priorityRatingData.CaseAllocationManager}");
                Logger.WriteLog($"Priority Rating Manager Decision:{priorityRatingData.PriorityRatingManagerDecision}");
                Logger.WriteLog($"Priority Rating Manager option:{priorityRatingData.PriorityRatingManagerOption}");
                Logger.WriteLog($"Priority Rating Manager Comments:{priorityRatingData.PriorityRatingManagerComments}");
                Logger.WriteLog("Priority Rating Amend Or Approve FINISHED ");
            }
        }

        public void IntegrationWithObjective(AppSubTestData integrationData)
        {
            if (integrationData.Integration.ToLower() == "yes")
            {
                Logger.WriteLog("Integration with objective STARTED");
                myDashboard = loginPage.LogIn(integrationData.UserName, integrationData.Password);

                myDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);

                integrateWithObjectivePage = (IntegrateWithObjectivePage)caseDashboard
                    .ClickRelatedActionFromRelatedActionTab(RelatedActions.IntegrateWithObjective);

                caseDashboard = integrateWithObjectivePage.EnterFileNumber(integrationData.IntegrationFileNumber)
                    .SelectNoCaseFolderOption()
                    .EnterComments(integrationData.IntegrateWithObjectiveComments)
                    .ClickIntegrateButton()
                    .ClickDoneButton();

                caseDashboard.LogOut();
                Logger.WriteLog($"Integration with Objective Data - ");
                Logger.WriteLog($"Perform Integration with Objective?:{integrationData.Integration}, ");
                Logger.WriteLog($"Objective File Number:{integrationData.IntegrationFileNumber}");
                Logger.WriteLog("Integration with objective FINISHED");
            }
        }
        public void CreateTask(AppSubTestData createTaskData)
        {
            if (createTaskData.CreateTask.ToLower() == "yes")
            {
                Logger.WriteLog("Create Task 1 STARTED");
                myDashboard = loginPage.LogIn(createTaskData.UserName, createTaskData.Password);

                myDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = myDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);

                createTaskPage = (CreateTaskPage)caseDashboard
                    .ClickRelatedActionFromRelatedActionTab(RelatedActions.CreateTask);

                if (createTaskData.NoOfCreateTasks != "")
                    TaskCreation(createTaskPage, createTaskData.CreateTaskAllocatedOfficer1,
                    createTaskData.CreateTasksWorkInstruction1, createTaskData.CreateTasksTaskName1,
                    createTaskData.CreateTasksComments1);

                Logger.WriteLog("Create Task 2 STARTED");
                createTaskPage = (CreateTaskPage)caseDashboard
                    .ClickRelatedActionFromRelatedActionTab(RelatedActions.CreateTask);

                if (createTaskData.NoOfCreateTasks == "2")
                    TaskCreation(createTaskPage, createTaskData.CreateTaskAllocatedOfficer2,
                    createTaskData.CreateTasksWorkInstruction2, createTaskData.CreateTasksTaskName2,
                    createTaskData.CreateTasksComments2);

                caseDashboard.LogOut();
                Logger.WriteLog($"Create Task Data - ");
                Logger.WriteLog($"Create A Task?:{createTaskData.CreateTask}, ");
                Logger.WriteLog($"Number of Tasks to be created:{createTaskData.NoOfCreateTasks}, ");
                Logger.WriteLog($"Officer1:{createTaskData.CreateTaskAllocatedOfficer1}, ");
                Logger.WriteLog($"WI1:{createTaskData.CreateTasksWorkInstruction1}, ");
                Logger.WriteLog($"Task Name1:{createTaskData.CreateTasksTaskName1}, ");
                Logger.WriteLog($"Comments1:{createTaskData.CreateTasksComments1},");
                Logger.WriteLog($"Create Task Data - ");
                Logger.WriteLog($"Create A Task?:{createTaskData.CreateTask}, ");
                Logger.WriteLog($"Number of Tasks to be created:{createTaskData.NoOfCreateTasks}, ");
                Logger.WriteLog($"Officer1:{createTaskData.CreateTaskAllocatedOfficer1}, ");
                Logger.WriteLog($"WI1:{createTaskData.CreateTasksWorkInstruction1}, ");
                Logger.WriteLog($"Task Name1:{createTaskData.CreateTasksTaskName1}, ");
                Logger.WriteLog($"Comments1:{createTaskData.CreateTasksComments1},");
                Logger.WriteLog("Create Task FINISHED");
            }
        }
        public void TaskCreation(CreateTaskPage createTaskPage, string officerName,
            string WI, string taskName, string comments)
        {
            caseDashboard = createTaskPage.EnterOfficerName(officerName)
                .EnterWI(WI)
                .EnterTaskName(taskName)
                .EnterComments(comments)
                .SelectNoUploadOption()
                .ClickDoneButton();
        }

        public void FinaliseCase(AppSubTestData finaliseCaseData)
        {
            if (finaliseCaseData.FinaliseCase.ToLower() == "yes")
            {
                Logger.WriteLog("Finalise Case STARTED");
                myDashboard = loginPage.LogIn(finaliseCaseData.CaseAllocationManager, finaliseCaseData.CaseAllocationManagerPassword);

                teamDashboard = myDashboard.NavigateToTeamDashboard();

                teamDashboard.FilterAndSearchByCaseNumber(BasePage.CaseNumber);
                caseDashboard = teamDashboard.ClickCaseNumberTaskGridLink(BasePage.CaseNumber);

                //Finalise Case
                finaliseCase = (FinaliseCasePage)caseDashboard.ClickRelatedActionFromRelatedActionTab(RelatedActions.FinaliseCase);
                Assert.IsTrue(finaliseCase.IsPageLoadComplete());

                caseDashboard = finaliseCase
                    .SelectFinalisationReason(finaliseCaseData.FinalisationReason)
                    .EnterComments(finaliseCaseData.FinalisationComments)
                    .SelectNotToNotifyApplicantOrCustomer()
                    .TickMandatoryCheckboxes()
                    .ClickDoneButton();

                caseDashboard.LogOut();
                Logger.WriteLog("Finalise Case FINISHED");
            }
        }
    }
}
